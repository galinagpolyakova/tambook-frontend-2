# Tambook FrontEnd ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

## Installation

### Requirements

- NodeJS
- npm or yarn

`$ yarn install`

## Run

`$ yarn start`

## Test

### Test code style

`$ yarn lint`

### Test type validations

`$ yarn flow`

### Unit test

`$ yarn test`

### Test all

`$ yarn test:all`

## Deploy to AWS

`$ yarn build`
`$ yarn deploy`
