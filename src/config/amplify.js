export default {
  cognito: {
    identityPoolId: "us-east-1:2d71f707-80ef-4291-8702-b3b0ac4cab67",
    region: "us-east-1",
    identityPoolRegion: "us-east-1",
    userPoolId: "us-east-1_ld19dl7mO",
    userPoolWebClientId: "6pddpapf9q5qt1qhaa2bopddtt",
    mandatorySignIn: false
  },
  storage: {
    bucket: "tambook-img",
    region: "us-east-1"
  },
  social: {
    facebook: "2100535746941111",
    google:
      "919311818951-82jd4rkei0pnh32g2n3jsdn1nq8k2u8k.apps.googleusercontent.com"
  }
};
