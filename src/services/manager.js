import {
  registerGlobalServices,
  serviceManager
} from "../shared/services/manager";
import { CourseService } from "../containers/course/services";
import { ReviewService } from "../containers/review/services";
import { BookingService } from "../containers/booking/services";
import { GeneralService } from "../containers/general/services";
import { BlogService } from "../containers/blog/services";

export const registerServices = options => {
  registerGlobalServices(options);

  serviceManager.register("CourseService", serviceManager => {
    let api = serviceManager.get("ApiService");
    return new CourseService(api);
  });

  serviceManager.register("ReviewService", serviceManager => {
    let api = serviceManager.get("ApiService");
    return new ReviewService(api);
  });

  serviceManager.register("BookingService", serviceManager => {
    let api = serviceManager.get("ApiService");
    return new BookingService(api);
  });

  serviceManager.register("GeneralService", serviceManager => {
    let api = serviceManager.get("ApiService");
    return new GeneralService(api);
  });

  serviceManager.register("BlogService", serviceManager => {
    let api = serviceManager.get("ApiService");
    return new BlogService(api);
  });
};

export { serviceManager };
