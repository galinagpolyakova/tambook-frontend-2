// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import "./styles.css";

import * as countries from "../../shared/data/countries.json";
import * as languages from "../../shared/data/languages.json";

import Modal from "../../shared/components/Modal";
import Select from "../../shared/components/Select";
import Row from "../../shared/components/Row";
import Col from "../../shared/components/Col";
import Button from "../../shared/components/Button/Button";

import { UserSettingsConsumer } from "../../contexts/UserSettings";
import {
  getCurrencyList,
  getUserPreferences,
  saveUserPreferences,
  getCurrencyRate
} from "../../containers/profile/store/actions";

type UserPreferencesProps = {
  showModal: boolean,
  onClose: Function,
  userSettings: {
    language: string,
    currency: string,
    passport: string
  },
  currencies: Object,
  getCurrencyList: Function,
  getCurrencyRate: Function,
  saveUserPreferences: Function,
  getUserPreferences: Function
};

type UserPreferencesState = {
  language: string,
  passport: string,
  currency: string
};

class UserPreferences extends Component<
  UserPreferencesProps,
  UserPreferencesState
  > {
  constructor(props) {
    super(props);

    this.state = {
      language: "",
      passport: "",
      currency: ""
    };
    // $FlowFixMe
    this.saveUserSettings = this.saveUserSettings.bind(this);
  }

  componentDidMount() {
    this.props.getUserPreferences();
    const {
      userSettings: { language, passport, currency },
      currencies
    } = this.props;
    if (currencies === null) {
      this.props.getCurrencyList();
      this.props.getCurrencyRate(currency);
    }
    this.setState({
      language,
      passport,
      currency
    });
  }

  componentDidUpdate(prevProps) {
    const {
      userSettings: { currency }
    } = this.props;
    if (prevProps.userSettings.currency !== currency) {
      this.props.getCurrencyRate(currency);
    }
  }

  saveUserSettings() {
    this.props.saveUserPreferences(this.state);
    this.props.onClose();
  }

  render() {
    const { showModal, onClose, currencies } = this.props;
    const { language, passport, currency } = this.state;
    const countryList = [];

    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }

    const currencyList = [];
    for (const key in currencies) {
      if (currencies.hasOwnProperty(key)) {
        currencyList.push({
          value: key,
          name: currencies[key]
        });
      }
    }

    const languageList = [];
    for (const key in languages) {
      if (languages.hasOwnProperty(key)) {
        languageList.push({
          value: key,
          name: languages[key]
        });
      }
    }

    return (
      <Modal className="auth-modal" showModal={showModal} onClose={onClose}>
        <div className="user-preferences">
          <div className="heading">
            Tambook best offers are based on your citizenship. Please select
            website language, passport and preferred currency.
          </div>
          <div className="form-block">
            <Row className="settings-block">
              <Col>
                <label>Language</label>
                <Select
                  options={["English"]}
                  placeholder="Language"
                  selected={language}
                  onChange={language => this.setState({ language })}
                  name="language"
                />
              </Col>
              <Col>
                <label>Passport</label>
                <Select
                  options={countryList}
                  placeholder="Passport"
                  selected={passport}
                  onChange={passport => this.setState({ passport })}
                  searchable
                  name="passport"
                />
              </Col>
              <Col>
                <label>Currency</label>
                <Select
                  options={currencyList}
                  placeholder="Currency"
                  selected={currency}
                  onChange={currency => this.setState({ currency })}
                  searchable
                  name="currency"
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Button onClick={() => this.saveUserSettings()}>
                  Continue
                </Button>
              </Col>
            </Row>
          </div>
          <div className="notice">
            By choosing a country and currency above, you agree to our{" "}
            <a href="/under-construction">Terms and Conditions</a>.
          </div>
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    currencies: state.profile.currencies,
    userSettings: state.profile.settings
  };
}

const Actions = {
  getCurrencyList,
  getUserPreferences,
  saveUserPreferences,
  getCurrencyRate
};

const UserPreferencesContainer = connect(
  mapStateToProps,
  Actions
)(UserPreferences);

export default class UserPreferencesContext extends Component<{}> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ showUserPreference, toggleUserPreference }) => (
          <UserPreferencesContainer
            {...this.props}
            showModal={showUserPreference}
            onClose={toggleUserPreference}
          />
        )}
      </UserSettingsConsumer>
    );
  }
}
