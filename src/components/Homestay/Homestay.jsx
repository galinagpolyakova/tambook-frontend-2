// @flow
import { type ConsolidatedHomestayType } from "../../containers/course/types";

import React, { PureComponent } from "react";

import "./styles.css";
import Currency from "../Currency";
import Alert from "../../shared/components/Alert";

type HomestayProps = {
  title: $PropertyType<ConsolidatedHomestayType, "title">,
  image: $PropertyType<ConsolidatedHomestayType, "image">,
  above18Price: $PropertyType<ConsolidatedHomestayType, "above18Price">,
  under18Price: $PropertyType<ConsolidatedHomestayType, "under18Price">,
  homestayFee: $PropertyType<ConsolidatedHomestayType, "homestayFee">,
  currencyType: string
};

class Homestay extends PureComponent<HomestayProps> {
  render() {
    const {
      title,
      image,
      above18Price,
      under18Price,
      homestayFee,
      currencyType
    } = this.props;
    if (
      above18Price === null &&
      under18Price === null &&
      homestayFee === null
    ) {
      return <Alert>Coming Soon</Alert>;
    }
    return (
      <div className="homestay-group">
        <div className="homestay">
          <div className="homestay-image">
            <img alt={title} src={image} />
          </div>
          <div className="homestay-details">
            <div className="homestay-price">
              FROM{" "}
              <Currency currencyType={currencyType}>{above18Price}</Currency>
              &nbsp;/&nbsp;PW
            </div>
            <div className="homestay-title">{title}</div>
            <div className="homestay-age">for students 18 +</div>
          </div>
        </div>
        <div className="homestay">
          <div className="homestay-image">
            <img alt={title} src={image} />
          </div>
          <div className="homestay-details">
            <div className="homestay-price">
              FROM{" "}
              <Currency currencyType={currencyType}>{under18Price}</Currency>
              &nbsp;/&nbsp;PW
            </div>
            <div className="homestay-title">{title}</div>
            <div className="homestay-age">for students under 18</div>
          </div>
        </div>
        <div className="school-homestay">
          School&rsquo;s homestay placement fee
          <span className="pull-right">
            <Currency currencyType={currencyType}>{homestayFee}</Currency>
          </span>
        </div>
      </div>
    );
  }
}

export default Homestay;
