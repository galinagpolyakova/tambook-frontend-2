// @flow
import React, { PureComponent, type Element } from "react";
import classNames from "classnames";

import { isNotEmpty } from "../../shared/utils";

type WidgetProps = {
  title?: string,
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  type: | typeof Widget.types.DEFAULT
    | typeof Widget.types.PRIMARY
    | typeof Widget.types.INFO
    | typeof Widget.types.LIGHT
    | typeof Widget.types.SECONDARY,
  className: string
};

class Widget extends PureComponent<WidgetProps> {
  static types = {
    DEFAULT: "default",
    PRIMARY: "primary",
    INFO: "info",
    LIGHT: "light",
    SECONDARY: "secondary"
  };

  static defaultProps = {
    type: Widget.types.DEFAULT
  };

  render() {
    const { title, children, type, className } = this.props;
    return (
      <div className={classNames("widget", className, `widget-${type}`)}>
        {isNotEmpty(title) ? <div className="title">{title}</div> : ""}
        {children}
      </div>
    );
  }
}

export default Widget;
