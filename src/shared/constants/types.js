export const DEFAULT = "default";
export const PRIMARY = "primary";
export const INFO = "info";
export const LIGHT = "light";
export const SECONDARY = "secondary";
