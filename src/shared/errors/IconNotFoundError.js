// @flow
export class IconNotFoundError extends Error {
    constructor(library: string, icon: string) {
        super(`Icon '${icon}' can't be found in '${library}'`);
    }
}
