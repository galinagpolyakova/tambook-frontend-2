// @flow
import type { NetworkErrorInterface } from "./NetworkErrorInterface";

export default class NotFoundError extends Error implements NetworkErrorInterface<any> {
    _response: any;

    constructor(response: any) {
        super('404 error');
        this._response = response;
    }

    getResponse(): any {
        return this._response;
    }
}
