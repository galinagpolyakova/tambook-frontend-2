// @flow
export class ConsoleErrorInTests extends Error {
    args: Array<any>;

    constructor(warning: string, ...args: Array<any>) {
        super(warning);

        this.args = args;
    }
}
