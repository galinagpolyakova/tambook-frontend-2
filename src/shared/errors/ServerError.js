// @flow
import type { NetworkErrorInterface } from "./NetworkErrorInterface";

export default class ServerError extends Error implements NetworkErrorInterface<any> {
    _response: any;

    constructor(response: any) {
        super('Bad response from server');
        this._response = response;
    }

    getResponse(): any {
        return this._response;
    }
}
