// @flow
export default class InvalidUserEntryError extends Error {
    constructor(field: string) {
        super(`Please enter a valid ${field}.`);
    }
}
