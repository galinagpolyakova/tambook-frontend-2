import formatDistance from "./_lib/formatDistance";
import formatLong from "./_lib/formatLong";
import formatRelative from "../en-US/_lib/formatRelative";
import localize from "../en-US/_lib/localize";
import match from "../en-US/_lib/match";

/**
 * @type {Locale}
 * @category Locales
 * @summary English locale (Canada).
 * @language English
 * @iso-639-2 eng
 * @author Mark Owsiak [@markowsiak]{@link https://github.com/markowsiak}
 * @author Marco Imperatore [@mimperatore]{@link https://github.com/mimperatore}
 */
var locale = {
  formatDistance: formatDistance,
  formatLong: formatLong,
  formatRelative: formatRelative,
  localize: localize,
  match: match,
  options: {
    weekStartsOn: 0 /* Sunday */,
    firstWeekContainsDate: 1
  }
};

export default locale;
