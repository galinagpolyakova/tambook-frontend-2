import formatDistance from "../en-US/_lib/formatDistance";
import formatLong from "./_lib/formatLong";
import formatRelative from "../en-US/_lib/formatRelative";
import localize from "../en-US/_lib/localize";
import match from "../en-US/_lib/match";

/**
 * @type {Locale}
 * @category Locales
 * @summary English locale (United Kingdom).
 * @language English
 * @iso-639-2 eng
 * @author Alex [@glintik]{@link https://github.com/glintik}
 */
var locale = {
  formatDistance: formatDistance,
  formatLong: formatLong,
  formatRelative: formatRelative,
  localize: localize,
  match: match,
  options: {
    weekStartsOn: 1 /* Monday */,
    firstWeekContainsDate: 4
  }
};

export default locale;
