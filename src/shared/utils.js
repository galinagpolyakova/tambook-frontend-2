let globalCssModule;

export function setGlobalCssModule(cssModule) {
  globalCssModule = cssModule;
}

export function mapToCssModules(className = "", cssModule = globalCssModule) {
  if (!cssModule) return className;
  return className
    .split(" ")
    .map(c => cssModule[c] || c)
    .join(" ");
}

let warned = {};

export function warnOnce(message) {
  if (!warned[message]) {
    /* istanbul ignore else */
    if (typeof console !== "undefined") {
      console.error(message); // eslint-disable-line no-console
    }
    warned[message] = true;
  }
}

export function deprecated(propType, explanation) {
  return function validate(props, propName, componentName, ...rest) {
    if (props[propName] !== null && typeof props[propName] !== "undefined") {
      warnOnce(
        `"${propName}" property of "${componentName}" has been deprecated.\n${explanation}`
      );
    }

    return propType(props, propName, componentName, ...rest);
  };
}

export function isEmpty(string) {
  return string === null || string === "" || string === undefined;
}

export function isNotEmpty(string) {
  return !isEmpty(string);
}

export function textTruncate(str = "", length = 100, ending = "...") {
  if (str === null || str === undefined) {
    return "";
  }
  if (str.length > length) {
    return str.substring(0, length - ending.length) + ending;
  } else {
    return str;
  }
}

export function hasWhiteSpace(s) {
  return /\s/g.test(s);
}

export function isString(s) {
  typeof s === "string" || s instanceof String;
}

export function isCompleteUrl(str) {
  return (str.substring(0, 1) == "/");
}