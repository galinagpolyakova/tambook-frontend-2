// @flow
export interface SerializableInterface<Output> {
    toJSON(): Output
}
