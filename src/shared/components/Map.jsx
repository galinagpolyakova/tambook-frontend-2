// @flow
import React, { Component } from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";

const mapStyles = {
  width: "100%",
  height: "200px"
};

const mapContainerStyles = {
  minHeight: "200px",
  width: "100%",
  position: "relative"
};

type MapContainerProps = {
  lat: string,
  lng: string,
  google: string
};

export class MapContainer extends Component<MapContainerProps> {
  render() {
    const { lat, lng, google } = this.props;
    return (
      <div style={mapContainerStyles}>
        <Map
          google={google}
          zoom={14}
          style={mapStyles}
          initialCenter={{
            lat,
            lng
          }}
        >
          <Marker
            title={"The marker`s title will appear as a tooltip."}
            name={"SOMA"}
            position={{ lat, lng }}
          />
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyALKoX7ULIdj3eNEErooYtP4ECwpZ0L_X8"
})(MapContainer);
