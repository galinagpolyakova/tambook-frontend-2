// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";
import { isNotEmpty } from "../../utils";

type SectionHeaderProps = {
  heading: any,
  subheading?: string,
  position: string
};

class SectionHeader extends PureComponent<SectionHeaderProps> {
  static defaultProps = {
    position: "center"
  };
  render() {
    const { heading, subheading, position } = this.props;
    return (
      <div className={classNames("section-header", `title-${position}`)}>
        <div className="section-heading">{heading}</div>
        {isNotEmpty(subheading) ? (
          <div className="section-subheading">{subheading}</div>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default SectionHeader;
