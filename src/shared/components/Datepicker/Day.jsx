// @flow
import React, { PureComponent } from "react";
import format from "../../utils/dates/format";
import isToday from "../../utils/dates/isToday";
import classNames from "classnames";

type DayProps = {
  day: Date | null,
  isSelected: boolean,
  isDisabled: boolean,
  onClick: (day: Date) => void,
  readonly: boolean
};

export class Day extends PureComponent<DayProps> {
  constructor(props: DayProps) {
    super(props);

    //$FlowFixMe Perfs
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e: SyntheticEvent<HTMLElement>) {
    e.preventDefault();
    if (this.props.day === null) {
      return;
    }
    this.props.onClick(this.props.day);
  }

  render() {
    if (this.props.day === null) {
      return <span className="day blank" />;
    }

    let day = this.props.day;

    return (
      <button
        className={classNames(
          "btn btn-link day",
          format(this.props.day, "dddd").toLowerCase(),
          {
            disabled: this.props.day && this.props.isDisabled,
            selected: this.props.day && this.props.isSelected,
            today: this.props.day && isToday(this.props.day)
          }
        )}
        disabled={this.props.readonly || this.props.isDisabled}
        onClick={this.handleClick}
      >
        <span className="day-text">{format(day, "dd")}</span>
      </button>
    );
  }
}
