// @flow
import React, { PureComponent } from "react";
import { Day } from "./Day";
import isSameDay from "../../utils/dates/isSameDay";

type WeekProps = {
  week: Array<null | Date>,
  onDayClick: (day: Date) => void,
  selectedDate: Date | null,
  readonly: boolean,
  availableDates: Date[]
};

export class Week extends PureComponent<WeekProps> {
  static defaultProps = {
    onDayClick: () => {}
  };

  isSelected(day: Date | null): boolean {
    if (day === null) {
      return false;
    }
    return isSameDay(day, this.props.selectedDate);
  }

  isDisabled(day: Date | null): boolean {
    if (day === null) {
      return false;
    }

    return (
      this.props.availableDates.find((currentSelectedDay: Date) => {
        return isSameDay(day, currentSelectedDay);
      }) === undefined
    );
  }

  render() {
    return (
      <div className="week">
        {this.props.week.map((day: Date | null, i) => {
          return (
            <Day
              day={day}
              key={`day-${i}`}
              readonly={this.props.readonly}
              isDisabled={this.isDisabled(day)}
              isSelected={this.isSelected(day)}
              onClick={this.props.onDayClick}
            />
          );
        })}
      </div>
    );
  }
}
