// @flow
import type { Element } from "react";
import React, { PureComponent } from "react";

type CardHeaderProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined
};

export default class CardHeader extends PureComponent<CardHeaderProps> {
  render() {
    return <div className="card-header">{this.props.children}</div>;
  }
}
