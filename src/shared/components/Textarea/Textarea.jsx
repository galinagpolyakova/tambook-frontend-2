// @flow
import React, { PureComponent } from 'react';

import classNames from "classnames";

import "./styles.css";

type TextareaProps = {
  name?: string,
  text: string,
  id?: string,
  placeholder: string,
  text: string,
  onChange: Function,
  error: null | string,
  className: string,
  rows?: number
}
class Textarea extends PureComponent<TextareaProps> {
  static defaultProps = {
    error: null,
    className: "",
    onChange: () => { },
    placeholder: "",
    rows: 10,
    text: ""
  };

  getFieldErrors(error: string | null) {
    return error !== null ? (
      <ul className="form-errors">
        <li>{error}</li>
      </ul>
    ) : (
        ""
      );
  }
  render() {
    const {
      placeholder,
      name,
      id,
      rows,
      text,
      onChange,
      error,
      className
    } = this.props;
    const hasErrors = error !== null;
    return (
      <div
        className={classNames(
          "form-input",
          { "has-errors": hasErrors },
          className
        )}
      >
        <textarea
          name={name}
          id={id}
          rows={rows}
          placeholder={placeholder}
          onChange={onChange}
          value={text}
        >
        </textarea>
        {this.getFieldErrors(error)}
      </div>
    );
  }
}

export default Textarea;
