// @flow
import React, { PureComponent } from "react";
import Button from "../Button";
import Icon from "../Icon";

import "./styles.css";

type SortButtonProps = {
  onClick: Function,
  filter: string,
  children: Node
};

type SortButtonState = {
  sort: typeof SortButton.type.ASC | typeof SortButton.type.DESC | null
};

class SortButton extends PureComponent<SortButtonProps, SortButtonState> {
  static type = {
    ASC: "asc",
    DESC: "desc"
  };

  state = {
    sort: null
  };

  toggleSort = () => {
    const sort =
      this.state.sort === SortButton.type.ASC || this.state.sort === null
        ? SortButton.type.DESC
        : SortButton.type.ASC;
    this.setState({
      sort
    });
    this.props.onClick({ [this.props.filter]: sort });
  };

  getIcon = () => {
    switch (this.state.sort) {
      case SortButton.type.ASC:
        return <Icon icon="sort-down" />;
      case SortButton.type.DESC:
        return <Icon icon="sort-up" />;
      default:
        return <Icon icon="sort" />;
    }
  };
  render() {
    const { children } = this.props;
    return (
      <Button
        className="sort-button"
        type={Button.type.LIGHT}
        size={Button.size.SMALL}
        onClick={() => this.toggleSort()}
      >
        {children}
        {this.getIcon()}
      </Button>
    );
  }
}

export default SortButton;
