// @flow
import React, { PureComponent } from "react";

import classNames from "classnames";
import Star from "./Star";

import "./styles.css";

type RateProps = {
  value: number,
  size: | typeof Rate.size.SMALL
    | typeof Rate.size.MEDIUM
    | typeof Rate.size.LARGE
};

class Rate extends PureComponent<RateProps> {
  static size = {
    SMALL: "rate-small",
    MEDIUM: "rate-medium",
    LARGE: "rate-large"
  };
  static defaultProps = {
    value: 0,
    size: Rate.size.MEDIUM
  };
  render() {
    const stars = [];
    const { value, size } = this.props;
    for (let index = 1; index <= 5; index++) {
      stars.push(<Star key={index} isFilled={index <= value} isHalf={false} />);
    }
    return <div className={classNames("star-rate", `${size}`)}>{stars}</div>;
  }
}

export default Rate;
