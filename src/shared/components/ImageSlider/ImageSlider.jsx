// @flow
import React, { PureComponent } from "react";

import Slide from "./Slide";
import SliderControls from "./SliderControls";

import "./styles.css";

type SliderProps = {
  slides: Array<string>,
  autoplay: boolean
};
type SliderState = {
  currentIndex: number,
  translateValue: number,
  interval: null | number
};
export default class Slider extends PureComponent<SliderProps, SliderState> {
  state = {
    currentIndex: 0,
    translateValue: 0,
    interval: null
  };

  goToPreviousSlide = () => {
    const { currentIndex, translateValue } = this.state;

    if (currentIndex === 0) {
      const lastIndex = this.props.slides.length - 1;
      this.setTranslateValue(-lastIndex * this.slideWidth());
      this.setIndex(lastIndex);
      return;
    }

    this.setTranslateValue(translateValue + this.slideWidth());
    this.setIndex(currentIndex - 1);
  };

  goToNextSlide = () => {
    const { currentIndex, translateValue } = this.state;
    const { slides } = this.props;

    if (currentIndex === slides.length - 1) {
      this.setTranslateValue(0);
      return this.setIndex(0);
    }

    this.setTranslateValue(translateValue - this.slideWidth());
    this.setIndex(currentIndex + 1);
  };

  setIndex = (currentIndex: number) => {
    this.setState({
      currentIndex
    });
  };

  setTranslateValue = (translateValue: number) => {
    this.setState({
      translateValue
    });
  };

  handleSlideClick = (index: number) => {
    const { currentIndex, translateValue } = this.state;
    if (index === currentIndex) return;
    if (index > currentIndex) {
      this.setTranslateValue(-index * this.slideWidth());
    } else {
      this.setTranslateValue(
        translateValue + (currentIndex - index) * this.slideWidth()
      );
    }
    this.setIndex(index);
  };

  componentDidUpdate = (prevProps: SliderProps) => {
    const { autoplay } = this.props;

    if (autoplay && prevProps.autoplay !== autoplay) {
      const interval = window.setInterval(() => {
        this.goToNextSlide();
      }, 3000);
      this.setState({ interval });
    } else if (!autoplay && prevProps.autoplay !== autoplay) {
      const interval = window.clearInterval(this.state.interval);
      this.setState({ interval });
    }
  };

  slideWidth = () => {
    // $FlowFixMe
    return document.querySelector(".slide").clientWidth;
  };

  render() {
    const { slides } = this.props;
    return (
      <div className="slider-cotainer">
        <div className="slider">
          <div
            className="slider-wrapper"
            style={{
              transform: `translateX(${this.state.translateValue}px)`,
              transition: "transform ease-out 0.45s"
            }}
          >
            {slides.map((image, i) => (
              <Slide key={i} image={image} />
            ))}
          </div>
          <Arrow side="prev" handleClick={this.goToPreviousSlide} />
          <Arrow side="next" handleClick={this.goToNextSlide} />
        </div>
        <SliderControls slides={slides} handleClick={this.handleSlideClick} />
      </div>
    );
  }
}

type ArrowProps = {
  handleClick: Function,
  side: "next" | "prev"
};

class Arrow extends PureComponent<ArrowProps> {
  render() {
    const { side, handleClick } = this.props;
    const className =
      side === "next" ? "slider-arrow arrow-next" : "slider-arrow arrow-prev";
    const image =
      side === "next"
        ? "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjkgMTI5IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjkgMTI5IiB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4Ij4KICA8Zz4KICAgIDxnPgogICAgICA8cGF0aCBkPSJNNjQuNSwxMjIuNmMzMiwwLDU4LjEtMjYsNTguMS01OC4xUzk2LjUsNi40LDY0LjUsNi40UzYuNCwzMi41LDYuNCw2NC41UzMyLjUsMTIyLjYsNjQuNSwxMjIuNnogTTY0LjUsMTQuNiAgICBjMjcuNSwwLDQ5LjksMjIuNCw0OS45LDQ5LjlTOTIsMTE0LjQsNjQuNSwxMTQuNFMxNC42LDkyLDE0LjYsNjQuNVMzNywxNC42LDY0LjUsMTQuNnoiIGZpbGw9IiNGRkZGRkYiLz4KICAgICAgPHBhdGggZD0ibTUxLjEsOTMuNWMwLjgsMC44IDEuOCwxLjIgMi45LDEuMiAxLDAgMi4xLTAuNCAyLjktMS4ybDI2LjQtMjYuNGMwLjgtMC44IDEuMi0xLjggMS4yLTIuOSAwLTEuMS0wLjQtMi4xLTEuMi0yLjlsLTI2LjQtMjYuNGMtMS42LTEuNi00LjItMS42LTUuOCwwLTEuNiwxLjYtMS42LDQuMiAwLDUuOGwyMy41LDIzLjUtMjMuNSwyMy41Yy0xLjYsMS42LTEuNiw0LjIgMCw1Ljh6IiBmaWxsPSIjRkZGRkZGIi8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K"
        : "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjkgMTI5IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjkgMTI5IiB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4Ij4KICA8Zz4KICAgIDxnPgogICAgICA8cGF0aCBkPSJtNjQuNSwxMjIuNmMzMiwwIDU4LjEtMjYgNTguMS01OC4xcy0yNi01OC01OC4xLTU4LTU4LDI2LTU4LDU4IDI2LDU4LjEgNTgsNTguMXptMC0xMDhjMjcuNSw1LjMyOTA3ZS0xNSA0OS45LDIyLjQgNDkuOSw0OS45cy0yMi40LDQ5LjktNDkuOSw0OS45LTQ5LjktMjIuNC00OS45LTQ5LjkgMjIuNC00OS45IDQ5LjktNDkuOXoiIGZpbGw9IiNGRkZGRkYiLz4KICAgICAgPHBhdGggZD0ibTcwLDkzLjVjMC44LDAuOCAxLjgsMS4yIDIuOSwxLjIgMSwwIDIuMS0wLjQgMi45LTEuMiAxLjYtMS42IDEuNi00LjIgMC01LjhsLTIzLjUtMjMuNSAyMy41LTIzLjVjMS42LTEuNiAxLjYtNC4yIDAtNS44cy00LjItMS42LTUuOCwwbC0yNi40LDI2LjRjLTAuOCwwLjgtMS4yLDEuOC0xLjIsMi45czAuNCwyLjEgMS4yLDIuOWwyNi40LDI2LjR6IiBmaWxsPSIjRkZGRkZGIi8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K";
    return (
      <div className={className} onClick={handleClick}>
        <img alt={className} src={image} />
      </div>
    );
  }
}
