// @flow
import React, { PureComponent } from "react";

type SlideProps = {
  image: string
};

class Slide extends PureComponent<SlideProps> {
  render() {
    const styles = {
      backgroundImage: `url(${this.props.image})`,
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "50% 60%"
    };
    return <div className="slide" style={styles} />;
  }
}

export default Slide;
