// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import { CardHeader, CardBody } from "../Card";

import "./styles.css";

export type AccordionType = {
  title: string,
  content: string
};

type AccordionProps = {
  items: Array<AccordionType>
};

type AccordionState = {
  activeKey: Array<number>
};

class Accordion extends PureComponent<AccordionProps, AccordionState> {
  state = {
    activeKey: []
  };

  onClickHandler = (key: number) => {
    let activeKey = [...this.state.activeKey];

    const index = activeKey.indexOf(key);
    if (index > -1) {
      activeKey.splice(index, 1);
    } else {
      activeKey.push(key);
    }
    this.setState({ activeKey });
  };

  render() {
    const { items } = this.props;
    const { activeKey } = this.state;
    const accordionItems = items.map((item, key) => {
      const isActive = activeKey.indexOf(key) > -1;
      return (
        <div
          key={key}
          className={classNames("accordion-item", { active: isActive })}
        >
          <CardHeader>
            <a onClick={() => this.onClickHandler(key)}>
              <div className="toggler">
                <i className="icon" />
              </div>
              {item.title}
            </a>
          </CardHeader>
          <CardBody>{item.content}</CardBody>
        </div>
      );
    });
    return <div className="accordion">{accordionItems}</div>;
  }
}

export default Accordion;
