// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";
import type { ReviewsListType } from "./types";
import { Review } from "./model";

export class ReviewService {
  api: ApiServiceInterface;

  endpoint: string = "/reviews";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _normalizeReviews(apiReviews: any): ReviewsListType {
    const reviews = [];
    apiReviews.map(apiReview => {
      let review = Review.fromReviewApi(apiReview);
      reviews.push(review);
    });
    return reviews;
  }

  getFeaturedReviews() {
    return this.api._fetch("get", this.endpoint).then(response => {
      return this._normalizeReviews(response.result.Items);
    });
  }
}
