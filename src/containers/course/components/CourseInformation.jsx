// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Parser from "html-react-parser";

import { getCourseActivities, getCourseFacilities } from "../store/actions";

import Tabs from "../../../shared/components/Tabs";
import Section from "../../../shared/components/Section";

type CourseInformationProps = {
  getCourseActivities: Function,
  getCourseFacilities: Function,
  information: {
    description: string,
    activities: string[],
    facilities: string[]
  },
  activities: Array<{
    name: string,
    image: string
  }>,
  facilities: Array<{
    name: string,
    image: string
  }>
};

class CourseInformation extends PureComponent<CourseInformationProps> {
  componentDidMount() {
    const {
      information: { activities, facilities }
    } = this.props;
    if (activities !== undefined && activities.length > 0) {
      this.props.getCourseActivities({
        list: activities.toString()
      });
    }
    if (facilities !== undefined && facilities.length > 0) {
      this.props.getCourseFacilities({
        list: facilities.toString()
      });
    }
  }
  render() {
    const { information, activities, facilities } = this.props;
    const activitiesContent = activities.map((activity, key) => (
      <div className="facility" key={key}>
        <img src={activity.image} />
        <span>{activity.name}</span>
      </div>
    ));

    const facilitiesContent = facilities.map((activity, key) => (
      <div className="facility" key={key}>
        <img src={activity.image} />
        <span>{activity.name}</span>
      </div>
    ));
    const items = [
      {
        title: "DESCRIPTION",
        content: Parser(information.description)
      },
      {
        title: "ACTIVITIES",
        content: (
          <div className="facilities-list">
            {activitiesContent}
            <div className="clearfix" />
          </div>
        )
      },
      {
        title: "FACILITIES",
        content: (
          <div className="facilities-list">
            {facilitiesContent}
            <div className="clearfix" />
          </div>
        )
      }
    ];
    return (
      <Section className="course-details">
        <Tabs items={items} />
      </Section>
    );
  }
}

function mapStateToProps(state) {
  return {
    activities: state.course.course.activities,
    facilities: state.course.course.facilities
  };
}

const Action = {
  getCourseActivities,
  getCourseFacilities
};

export default connect(
  mapStateToProps,
  Action
)(CourseInformation);
