// @flow
import React, { PureComponent, Fragment } from "react";

import { type CourseType, type CourseFeeType } from "../types";

import RadioButton from "../../../shared/components/RadioButton";
import Button from "../../../shared/components/Button";
import Widget from "../../../components/Sidebar/Widget";
import Select from "../../../shared/components/Select";
import Currency from "../../../components/Currency";
import Alert from "../../../shared/components/Alert";

type CourseFeeProps = {
  fees: $PropertyType<CourseType, "courseFees">,
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">,
  bookACourse: Function,
  discountList: $PropertyType<CourseType, "discountList">,
  passport: string,
  isPaymentEnabled: boolean,
  currencyType: $PropertyType<CourseType, "currencyType">
};

type CourseFeeState = {
  range: {
    weeksMin: number,
    weeksMax: number,
    price: number
  },
  isRangeSelected: boolean,
  selectedWeek: number | null,
  selectedRange: number,
  isFixedPrice: boolean
};

class CourseFee extends PureComponent<CourseFeeProps, CourseFeeState> {
  constructor() {
    super();

    this.state = {
      range: {
        weeksMin: 0,
        weeksMax: 0,
        price: 0
      },
      selectedRange: 0,
      isRangeSelected: false,
      selectedWeek: 0,
      isFixedPrice: false,
      discount: 0
    };
  }

  onRangeSelect = (selectedRange: number) => {
    let range = this.props.fees[selectedRange];
    if (range.weeksMin === null) {
      this.setState({
        isFixedPrice: true,
        selectedWeek: 0,
        selectedRange
      });
    }
    this.setState({
      range,
      isRangeSelected: true,
      selectedRange,
      selectedWeek: 0
    });
  };

  componentDidMount() {
    this.onRangeSelect(0);
  }

  getWeekRange = (
    min: $PropertyType<CourseFeeType, "weeksMin">,
    max: $PropertyType<CourseFeeType, "weeksMax">
  ) => {
    let ranges = [];

    let start = min === null ? 1 : parseInt(min);
    const end = max === null ? 50 : parseInt(max);

    while (start <= end) {
      ranges.push(start);
      start++;
    }

    return ranges;
  };

  render() {
    const {
      fees,
      enrolmentFee,
      discountList,
      bookACourse,
      passport,
      isPaymentEnabled,
      currencyType
    } = this.props;
    const {
      range: { weeksMin, weeksMax, price },
      isRangeSelected,
      selectedRange,
      isFixedPrice,
      selectedWeek
    } = this.state;
    let discount = 0;

    const options = this.getWeekRange(weeksMin, weeksMax);

    discountList.map(discountItem => {
      if (discountItem.weeksMin === null && discountItem.weeksMax === null) {
        if (discountItem.countryOfOrigin.includes(passport)) {
          discount = discountItem.discountedPrice;
        }
      } else {
        if (selectedWeek !== null) {
          if (
            discountItem.weeksMin <= selectedWeek &&
            discountItem.weeksMax >= selectedWeek &&
            discountItem.countryOfOrigin.includes(passport)
          ) {
            discount = discountItem.discountedPrice;
          }
        }
      }
    });

    if (selectedWeek === 0) {
      this.setState({
        selectedWeek: options[0]
      });
    }

    return (
      <Widget title="COURSE FEES" className="course-fees">
        <ul className="fees-list">
          <Fragment>
            {!isFixedPrice &&
              fees.map((courseFee, key) => (
                <li key={key} className="list-item">
                  <div className="durarion">
                    <RadioButton
                      onChange={() => this.onRangeSelect(key)}
                      value={key}
                      checked={key === selectedRange}
                      name="course-duration"
                      label={
                        courseFee.weeksMin === courseFee.weeksMax
                          ? `${courseFee.weeksMin} weeks`
                          : `${courseFee.weeksMin} - ${
                              courseFee.weeksMax
                            } weeks`
                      }
                    />
                  </div>
                  <div className="price">
                    <span>
                      <Currency currencyType={currencyType}>
                        {courseFee.price}
                      </Currency>
                      /
                    </span>
                    week
                  </div>
                </li>
              ))}
            {isRangeSelected && (
              <Fragment>
                <li className="list-item">
                  <span className="course-fee-title">
                    <Select
                      onChange={selectedWeek => this.setState({ selectedWeek })}
                      selected={selectedWeek}
                      options={options}
                    />
                    <span>weeks</span>
                  </span>
                  <span className="course-fee">
                    {" "}
                    <Currency currencyType={currencyType}>{price}</Currency>
                  </span>
                </li>
              </Fragment>
            )}
          </Fragment>

          {isRangeSelected && selectedWeek !== null && (
            <Fragment>
              <li className="list-item">
                <span className="course-fee-title">Course fee</span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {selectedWeek * price}
                  </Currency>
                </span>
              </li>
              {enrolmentFee !== 0 && enrolmentFee !== null && (
                <li className="list-item">
                  <span className="course-fee-title">
                    School registration fee
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {enrolmentFee}
                    </Currency>
                  </span>
                </li>
              )}
              {discount !== 0 && (
                <li className="list-item">
                  <span className="course-fee-title">Regular price</span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {selectedWeek * price + enrolmentFee}
                    </Currency>
                  </span>
                </li>
              )}
              {discount !== 0 && (
                <li className="list-item">
                  <span className="course-fee-title">Savings</span>
                  <span className="course-fee">
                    -{" "}
                    <Currency currencyType={currencyType}>
                      {selectedWeek * discount}
                    </Currency>
                  </span>
                </li>
              )}
              <li className="total list-item">
                <span className="course-fee-title">Total</span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {(price - discount) * selectedWeek + enrolmentFee}
                  </Currency>
                </span>
              </li>
            </Fragment>
          )}
        </ul>
        {isPaymentEnabled ? (
          <Button
            onClick={() => bookACourse(selectedWeek, selectedRange)}
            fullWidth
          >
            BOOK NOW
          </Button>
        ) : (
          <Alert>
            We are unable to book this course as the school has not completed
            the onboarding process.
          </Alert>
        )}
      </Widget>
    );
  }
}

export default CourseFee;
