// @flow
import React, { Component, Fragment } from "react";
import Accordion from "../../../shared/components/Accordion";
import Section, {
  SectionBody,
  SectionHeader
} from "../../../shared/components/Section";

type FaqSectionProps = {
  country: string
};

class FaqSection extends Component<FaqSectionProps> {
  getFaqItems = () => {
    const { country } = this.props;
    switch (country) {
      case "New Zealand":
        return [
          {
            title: "Do I need visa to study in New Zealand?",
            content: (
              <Fragment>
                <p>
                  If your course is less than three months in duration, you need
                  to apply for a visitor’s visa before coming to New Zealand,
                  unless you are from a visa - waiver country, in which case you
                  can get your visitor’s visa on arrival.Students on visitor’s
                  visa may study up to three months.Some of the countries that
                  are on the visa - waiver list are Brazil, Chile, Croatia,
                  Czech Republic, France, Germany, Hong Kong, Italy, Japan,
                  South Korea, Latvia, Poland, Saudi Arabia, Spain and
                  Switzerland.For a full list, please go to the website{" "}
                  <a href="https://www.immigration.govt.nz/new-zealand-visas">
                    www.immigration.govt.nz
                  </a>
                  .
                </p>
                <p>
                  If you are coming to study for more than three calendar
                  months, you need to apply for a student visa. Student visa
                  applications can be lodged online on&nbsp;
                  <a href="https://www.immigration.govt.nz/new-zealand-visas">
                    www.immigration.govt.nz
                  </a>
                  . If you are studying a short course on a visitor’s visa and
                  would like to study more than three months, you will need to
                  apply for a student visa for the duration of your next course.
                  You can change to student visa without leaving the country.
                </p>
              </Fragment>
            )
          },
          {
            title: "What do I need to apply for a student visa?",
            content: (
              <Fragment>
                <ol>
                  <li>Passport: a page with photo and your personal details</li>
                  <li>A recent photo 3.5x4.5 cm</li>
                  <li>An offer of place from a language school</li>
                  <li>
                    A bank statement in PDF format which confirms that you have
                    sufficient funds to cover the cost of your course and your
                    living expenses (1,250 NZD per month/15,000 NZD per year).
                  </li>
                  <li>
                    A job certificate. You need to provide it if&nbsp;the
                    following conditions are met: 1) you have a job; 2) the
                    duration of your course doesn’t exceed 6 months; 3) you are
                    not sponsored by anyone. Otherwise, this document is not
                    compulsory.
                  </li>
                  <li>
                    Certificates from your previous English courses. This
                    evidence of your previous language experience is
                    significant. Please, don’t ignore it!
                  </li>
                  <li>
                    Evidence of your tight connections with your home country: a
                    marriage certificate, birth certificates of your children, a
                    real estate certificate etc. These documents show that you
                    are a&nbsp; bona fide &nbsp;student who intends to come back
                    home after the end of a course.
                  </li>
                </ol>
                <p>
                  Pre-paid air tickets are not a compulsory part of your visa
                  package unless you are a citizen of a country with low student
                  visa approval rate (less than 80%). Go to&nbsp;
                  <a href="https://www.immigration.govt.nz/new-zealand-visas">
                    www.immigration.govt.nz
                  </a>
                  &nbsp; for more details.
                </p>
              </Fragment>
            )
          },
          {
            title: "Can I work while studying?",
            content: (
              <Fragment>
                <p>
                  If you are undertaking a short course on visitor’s visa, you
                  are not allowed to work. If you are applying for a student
                  visa, you may be allowed to work for up to 20 hours per week
                  if your course meets certain criteria, for example:
                </p>
                <ul>
                  <li>
                    If you are enrolled in an English language courses of at
                    least 14 weeks in duration at a Category 1 institution
                  </li>
                  <li>If your course is at least two years in duration</li>
                  <li>
                    If you are a tertiary exchange student and enrolled in a
                    course of at least one academic year
                  </li>
                </ul>
                <p>
                  You may be allowed to work full-time during vacations if you
                  are studying in a full-time programme of at least one academic
                  year in duration. Also, Masters by research and PhD students
                  have unlimited work rights.
                </p>
              </Fragment>
            )
          }
        ];
      case "Australia":
        return [
          {
            title: "Do I need visa to study in Australia?",
            content: (
              <Fragment>
                <p>
                  Yes, you must apply for a student visa regardless a duration
                  of your course and your citizenship. Light requirements are
                  applicable for New Zealand citizens only.
                </p>
                <p>
                  If you are coming to study for more than three calendar
                  months, you need to apply for a student visa. Student visa
                  applications can be lodged online on&nbsp;
                  www.immigration.govt.nz . If you are studying a short course
                  on a visitor’s visa and would like to study more than three
                  months, you will need to apply for a student visa for the
                  duration of your next course. You can change to student visa
                  without leaving the country.
                </p>
              </Fragment>
            )
          },
          {
            title: "What do I need to apply for a student visa?",
            content: (
              <Fragment>
                Yes, you must apply for a student visa regardless a duration of
                your course and your citizenship. Light requirements are
                applicable for New Zealand citizens only.
              </Fragment>
            )
          }
        ];
      default:
        return [];
    }
  };
  render() {
    const items = this.getFaqItems();

    if (items.length === 0) {
      return null;
    }

    return (
      <Section>
        <SectionHeader heading="IF YOU HAVE ANY QUESTIONS" position="left" />
        <SectionBody>
          <Accordion items={items} />
        </SectionBody>
      </Section>
    );
  }
}

export default FaqSection;
