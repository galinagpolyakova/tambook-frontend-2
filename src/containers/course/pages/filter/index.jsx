// @flow
import { type ConsolidatedCoursesListType } from "../../types";
import { type PaginationType } from "../../../../shared/components/Pagination";

import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Select from "../../../../shared/components/Select";

import Sidebar, { Widget } from "../../../../components/Sidebar";
import PageHeader from "../../../../components/PageHeader";
import Loader from "../../../../components/Loader";
import PriceFilter from "../../components/PriceFilter";
import FilterGroup from "../../components/FilterGroup";
import CourseView from "../../components/CourseView";

import {
  filterCourses,
  getFilterParams,
  getBookmarkCourseList,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
} from "../../store/actions";
import { isAuthenticated } from "../../../auth/store/selectors";
import {
  isCourseParamsLoaded,
  getConsolidatedCourses,
  getCourseParams,
  isCourseParamLoaded,
  isCourseBookmarksListLoaded
} from "../../store/selectors";
import { UserSettingsConsumer } from "../../../../contexts/UserSettings";

import {
  queryParamsParse,
  stringifyQueryParams
} from "../../../../shared/helpers/url";

import "rc-slider/assets/index.css";
import "./styles.css";
import Checkbox from "../../../../shared/components/Checkbox";

type CoursesFilterProps = {
  courses: ConsolidatedCoursesListType,
  filterCourses: Function,
  getFilterParams: Function,
  filters: Object,
  isLoaded: boolean,
  isParamsLoaded: boolean,
  location: {
    search: string
  },
  history: any,
  viewMode: any,
  coursesTotal: number,
  loading: boolean,
  pagination: PaginationType,
  isAuthenticated: boolean,
  isBookmarksListLoaded: boolean,
  getBookmarkCourseList: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  toggleAuth: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

class CoursesFilter extends PureComponent<CoursesFilterProps> {
  componentDidMount() {
    this.props.filterCourses(queryParamsParse(this.props.location.search));
    if (this.props.filters === null) {
      this.props.getFilterParams();
    }
  }

  componentDidUpdate() {
    if (this.props.isAuthenticated && !this.props.isBookmarksListLoaded) {
      this.props.getBookmarkCourseList();
    }
  }

  searchCourses = filter => {
    const query = {
      ...queryParamsParse(this.props.location.search),
      ...filter
    };
    const searchString = stringifyQueryParams(query);
    this.props.history.push({
      pathname: "/filter",
      search: searchString
    });
    this.props.filterCourses(query);
    this.props.getFilterParams(query);
  };

  toggleEligibleForWorkFilter = e => {
    e.preventDefault();
    // $FlowFixMe
    let { workDuringCourse } = queryParamsParse(this.props.location.search);
    const changedWorkDuringCourse = parseInt(workDuringCourse) === 1 ? 0 : 1;
    this.searchCourses({ workDuringCourse: changedWorkDuringCourse });
  };

  togglePremiumCoursesFilter = e => {
    e.preventDefault();
    // $FlowFixMe
    let { premiumCourses } = queryParamsParse(this.props.location.search);
    const changedPremiumCourses = parseInt(premiumCourses) === 1 ? 0 : 1;
    this.searchCourses({ premiumCourses: changedPremiumCourses });
  };

  isEligibleForWorkFilterActive = () => {
    // $FlowFixMe
    let { workDuringCourse } = queryParamsParse(this.props.location.search);
    return parseInt(workDuringCourse) === 1;
  };

  isPremiumCoursesFilterActive = () => {
    // $FlowFixMe
    let { premiumCourses } = queryParamsParse(this.props.location.search);
    return parseInt(premiumCourses) === 1;
  };

  getFilterContent = () => {
    const { isLoaded, filters } = this.props;
    // $FlowFixMe
    const {
      minPrice,
      maxPrice,
      country,
      language,
      courseType,
      courseIntensity,
      isEveningClass,
      schoolName
    } = queryParamsParse(this.props.location.search);
    if (isLoaded) {
      const courseTypeOptions = [
        {
          name: "All",
          value: ""
        },
        ...filters.courseType.map(filter => filter.type)
      ];
      const schoolNamesOptions = [
        {
          name: "All",
          value: ""
        },
        ...filters.schoolNames.map(filter => filter.schoolName)
      ];
      const courseScheduleOptions = [
        {
          name: "All",
          value: ""
        },
        ...filters.courseSchedule.map(filter => ({
          name: filter.schedule,
          value: filter.isEveningClass
        }))
      ];

      const courseIntensityOptions = [
        { name: "All", value: "" },
        { name: "Low(8—19 hours)", value: "8—19" },
        { name: "Standard(20—25 hours)", value: "20—25" },
        { name: "High(26—45 hours)", value: "26—45" }
      ];

      return (
        <Fragment>
          <Widget title="CATEGORIES">
            <div className="filter-list">
              <FilterGroup
                items={filters.languages}
                clickHandler={this.searchCourses}
                selected={language}
                title="LANGUAGES"
                filter="language"
              />
              <FilterGroup
                title="COUNTRIES"
                filter="country"
                selected={country}
                items={filters.countries}
                clickHandler={this.searchCourses}
              />
              <div className={classNames("filter-group")}>
                <div className="filter-title">ELIGIBLE FOR WORK</div>
                <ul className="filters">
                  <li>
                    <Checkbox
                      isChecked={this.isEligibleForWorkFilterActive()}
                      onChange={this.toggleEligibleForWorkFilter}
                      text="Yes"
                    />
                  </li>
                </ul>
              </div>
            </div>
          </Widget>
          <Widget title="ADVANCED FILTERS" className="advance-filters">
            <div className="form-group">
              <label>Select a course type</label>
              <Select
                name="course-type"
                options={courseTypeOptions}
                selected={courseType}
                onChange={courseType => this.searchCourses({ courseType })}
                defaultSelected={true}
                searchable
              />
            </div>
            <div className="form-group">
              <label>Select a course schedule</label>
              <Select
                name="course-schedule"
                options={courseScheduleOptions}
                selected={isEveningClass}
                onChange={isEveningClass =>
                  this.searchCourses({ isEveningClass })
                }
                defaultSelected={true}
                searchable
              />
            </div>
            <div className="form-group">
              <label>Course intensity</label>
              <Select
                name="course-intensity"
                placeholder="Course intensity"
                options={courseIntensityOptions}
                selected={courseIntensity}
                onChange={courseIntensity =>
                  this.searchCourses({ courseIntensity })
                }
                defaultSelected={true}
              />
            </div>
            <div className="form-group">
              <label>Select a school</label>
              <Select
                name="course-school"
                placeholder="Select a school"
                options={schoolNamesOptions}
                selected={schoolName}
                onChange={schoolName => this.searchCourses({ schoolName })}
                searchable
                defaultSelected={true}
              />
            </div>
          </Widget>
          <Widget title="weekly price range" className="price-filter">
            <PriceFilter
              searchCourses={this.searchCourses}
              minPrice={0}
              maxPrice={1500}
              selectedMinPrice={minPrice}
              selectedMaxPrice={maxPrice}
            />
          </Widget>
          <Widget title="PREMIUM COURSES" className="ratings-filter">
            <Checkbox
              isChecked={this.isPremiumCoursesFilterActive()}
              onChange={this.togglePremiumCoursesFilter}
              text="Yes"
            />
          </Widget>
        </Fragment>
      );
    }
  };

  addCourseToBookmarkList = courseID => {
    if (this.props.addCourseToBookmarkList !== undefined) {
      if (this.props.isAuthenticated) {
        this.props.addCourseToBookmarkList(courseID);
      } else {
        this.props.toggleAuth();
      }
    }
  };
  render() {
    const {
      courses,
      coursesTotal,
      loading,
      pagination,
      addCourseToCompare,
      removeCourseFromCompare
    } = this.props;

    return (
      <div className="page">
        <PageHeader
          title="FIND YOUR PERFECT COURSE"
          subheading={`WE FOUND: ${coursesTotal} COURSES`}
          breadcrumbs={[
            {
              title: "COURSES"
            }
          ]}
        />
        <div className="container">
          <Row>
            <Col size="3">
              <Sidebar>{this.getFilterContent()}</Sidebar>
            </Col>
            <Col>
              <Row>
                {loading ? (
                  <Loader isLoading />
                ) : (
                  <CourseView
                    filter={this.searchCourses}
                    courses={courses}
                    addCourseToBookmarkList={this.addCourseToBookmarkList}
                    removeCourseFromBookmarkList={
                      this.props.removeCourseFromBookmarkList
                    }
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                    pagination={pagination}
                    onPageChanged={this.searchCourses}
                  />
                )}
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    courses: getConsolidatedCourses(state),
    isLoaded: isCourseParamsLoaded(state),
    loading: state.course.loading,
    filters: getCourseParams(state),
    isParamsLoaded: isCourseParamLoaded(state),
    coursesTotal:
      state.course.pagination !== null ? state.course.pagination.items : 0,
    pagination: state.course.pagination,
    isAuthenticated: isAuthenticated(state),
    isBookmarksListLoaded: isCourseBookmarksListLoaded(state),
    compareList: state.course.compareList
  };
}

const Actions = {
  filterCourses,
  getFilterParams,
  getBookmarkCourseList,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
};

const CoursesFilterContainer = connect(
  mapStateToProps,
  Actions
)(CoursesFilter);

type CoursesFilterContextProps = {
  courses: ConsolidatedCoursesListType,
  filterCourses: Function,
  getFilterParams: Function,
  filters: Object,
  isLoaded: boolean,
  isParamsLoaded: boolean,
  location: {
    search: string
  },
  history: any,
  viewMode: any,
  coursesTotal: number,
  loading: boolean,
  pagination: PaginationType,
  isAuthenticated: boolean,
  isBookmarksListLoaded: boolean,
  getBookmarkCourseList: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

export default class CoursesFilterContext extends PureComponent<CoursesFilterContextProps> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth }) => (
          <CoursesFilterContainer {...this.props} toggleAuth={toggleAuth} />
        )}
      </UserSettingsConsumer>
    );
  }
}
