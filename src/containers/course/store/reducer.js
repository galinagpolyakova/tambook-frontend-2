// @flow
import { type Action } from "../../../shared/types/ReducerAction";
import { type PaginationType } from "../../../shared/components/Pagination";
import { saveData, loadData } from "../../../shared/kernel/localStorage";

import {
  GET_COURSE_INIT,
  GET_COURSE_SUCCESS,
  GET_COURSE_FAILURE,
  FILTER_COURSES_INIT,
  FILTER_COURSES_SUCCESS,
  FILTER_COURSES_FAILURE,
  GET_FILTER_PARAMS_INIT,
  GET_FILTER_PARAMS_SUCCESS,
  GET_FILTER_PARAMS_FAILURE,
  GET_FEATURED_COURSES_INIT,
  GET_FEATURED_COURSES_SUCCESS,
  GET_FEATURED_COURSES_FAILURE,
  GET_RELATED_COURSES_INIT,
  GET_RELATED_COURSES_SUCCESS,
  GET_RELATED_COURSES_FAILURE,
  GET_COURSE_ACTIVITIES_SUCCESS,
  GET_COURSE_FACILITIES_SUCCESS,
  ADD_COURSE_TO_BOOKMARK_LIST_INIT,
  ADD_COURSE_TO_BOOKMARK_LIST_FAILURE,
  ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS,
  GET_BOOKMARK_COURSE_LIST_SUCCESS,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS,
  LOAD_COURSES_IN_COMPARE,
  ADD_COURSE_TO_COMPARE,
  REMOVE_COURSE_FROM_COMPARE,
  GET_COMPARED_COURSE_LIST_SUCCESS
} from "./actionTypes";

export type CourseState = {
  submitting: boolean,
  notifications: null | String[],
  bookmarkList: String[] | null,
  course: Object,
  courses: Object[],
  filters: Object | null,
  coursesTotal: number,
  pagination: PaginationType,
  compareList: Object[]
};

const initialState: CourseState = {
  submitting: false,
  loading: false,
  isBookmarkLoading: false,
  isLoaded: false,
  bookmarkList: null,
  notifications: null,
  course: {
    id: null,
    data: null,
    relatedCources: [],
    activities: [],
    facilities: []
  },
  courses: [],
  filters: null,
  coursesTotal: 0,
  pagination: null,
  compareList: [],
  compareListCourse: []
};

const reducer = (
  state: CourseState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_COURSE_INIT:
      return getCourseInit(state);
    case GET_COURSE_SUCCESS:
      return getCourseSuccess(state, payload);
    case GET_COURSE_FAILURE:
      return getCourseFailure(state, payload);
    case GET_FEATURED_COURSES_INIT:
      return getFeaturedCoursesInit(state);
    case GET_FEATURED_COURSES_SUCCESS:
      return getFeaturedCoursesSuccess(state, payload);
    case GET_FEATURED_COURSES_FAILURE:
      return getFeaturedCoursesFailure(state, payload);
    case FILTER_COURSES_INIT:
      return fetchCoursesInit(state);
    case FILTER_COURSES_SUCCESS:
      return fetchCoursesSuccess(state, payload);
    case FILTER_COURSES_FAILURE:
      return fetchCoursesFailure(state);
    case GET_FILTER_PARAMS_INIT:
      return getFilterParamsInit(state);
    case GET_FILTER_PARAMS_SUCCESS:
      return getFilterParamsSuccess(state, payload);
    case GET_FILTER_PARAMS_FAILURE:
      return getFilterParamsFailure(state, payload);
    case GET_RELATED_COURSES_INIT:
      return getRelatedCoursesInit(state);
    case GET_RELATED_COURSES_SUCCESS:
      return getRelatedCoursesSuccess(state, payload);
    case GET_RELATED_COURSES_FAILURE:
      return getRelatedCoursesFailure(state, payload);
    case GET_COURSE_ACTIVITIES_SUCCESS:
      return getCourseActivitiesSuccess(state, payload);
    case GET_COURSE_FACILITIES_SUCCESS:
      return getCourseFacilitiesSuccess(state, payload);
    case ADD_COURSE_TO_BOOKMARK_LIST_INIT:
      return addCourseToBookmarkListInit(state);
    case ADD_COURSE_TO_BOOKMARK_LIST_FAILURE:
      return addCourseToBookmarkListFailure(state);
    case ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS:
      return addCourseToBookmarkListSuccess(state, payload);
    case GET_BOOKMARK_COURSE_LIST_SUCCESS:
      return getBookmarkCourseListSuccess(state, payload);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT:
      return removeCourseFromBookmarkListInit(state);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE:
      return removeCourseFromBookmarkListFailure(state);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS:
      return removeCourseFromBookmarkListSuccess(state, payload);
    case LOAD_COURSES_IN_COMPARE:
      return loadCoursesInCompare(state);
    case ADD_COURSE_TO_COMPARE:
      return addCourseToCompare(state, payload);
    case REMOVE_COURSE_FROM_COMPARE:
      return removeCourseFromCompare(state, payload);
    case GET_COMPARED_COURSE_LIST_SUCCESS:
      return getComparedCourseListSuccess(state, payload);
    default:
      return state;
  }
};

function getCourseInit(state: CourseState) {
  return {
    ...state,
    loading: true
  };
}

function getCourseSuccess(state: CourseState, { id, data }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    course: {
      id,
      data,
      relatedCources: [],
      activities: [],
      facilities: []
    }
  };
}

function getCourseFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    loading: false,
    courses: [],
    pagination: null,
    coursesTotal: 0
  };
}

function fetchCoursesInit(state: CourseState) {
  return {
    ...state,
    loading: true,
    isLoaded: false
  };
}

function fetchCoursesSuccess(
  state: CourseState,
  { courses, coursesTotal, pagination }
) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    courses,
    pagination,
    coursesTotal
  };
}

function fetchCoursesFailure(state: CourseState) {
  return {
    ...state,
    loading: false,
    isLoaded: true
  };
}

function getFilterParamsInit(state: CourseState) {
  return {
    ...state,
    loading: true,
    isLoaded: false,
    filters: null
  };
}

function getFilterParamsSuccess(state: CourseState, { filters }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    filters
  };
}

function getFilterParamsFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    loading: false,
    isLoaded: true
  };
}

function getFeaturedCoursesInit(state: CourseState) {
  return {
    ...state,
    loading: true,
    isLoaded: false
  };
}

function getFeaturedCoursesSuccess(state: CourseState, { featuredCourses }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    featuredCourses
  };
}

function getFeaturedCoursesFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    loading: false,
    isLoaded: true
  };
}

function getRelatedCoursesInit(state: CourseState) {
  return {
    ...state,
    loading: true,
    isLoaded: false
  };
}

function getRelatedCoursesSuccess(state: CourseState, { relatedCources }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    course: {
      ...state.course,
      relatedCources
    }
  };
}

function getRelatedCoursesFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    isLoaded: true,
    loading: false
  };
}

function getCourseActivitiesSuccess(state: CourseState, { activities }) {
  return {
    ...state,
    isLoaded: true,
    course: {
      ...state.course,
      activities
    }
  };
}

function getCourseFacilitiesSuccess(state: CourseState, { facilities }) {
  return {
    ...state,
    isLoaded: true,
    course: {
      ...state.course,
      facilities
    }
  };
}

function addCourseToBookmarkListInit(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: true
  };
}

function addCourseToBookmarkListFailure(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: false
  };
}

function addCourseToBookmarkListSuccess(state: CourseState, { id }) {
  const bookmarkList =
    state.bookmarkList === null ? [id] : [...state.bookmarkList, id];
  return {
    ...state,
    bookmarkList,
    isBookmarkLoading: false
  };
}

function getBookmarkCourseListSuccess(state: CourseState, bookmarkList) {
  return {
    ...state,
    bookmarkList
  };
}

function removeCourseFromBookmarkListInit(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: true
  };
}

function removeCourseFromBookmarkListFailure(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: false
  };
}

function removeCourseFromBookmarkListSuccess(state: CourseState, { id }) {
  return {
    ...state,
    // $FlowFixMe
    bookmarkList: state.bookmarkList.filter(courseId => courseId !== id),
    isBookmarkLoading: false
  };
}

function loadCoursesInCompare(state: CourseState) {
  const compareList =
    loadData("compareList") !== undefined ? loadData("compareList") : [];
  return {
    ...state,
    compareList
  };
}

function addCourseToCompare(state: CourseState, { id }) {
  const compareList = [...state.compareList, id];
  saveData("compareList", compareList);
  return {
    ...state,
    compareList
  };
}

function removeCourseFromCompare(state: CourseState, { id }) {
  // $FlowFixMe
  const compareList = state.compareList.filter(courseId => courseId !== id);
  saveData("compareList", compareList);
  return {
    ...state,
    compareList
  };
}

function getComparedCourseListSuccess(state, { compareListCourse }) {
  return {
    ...state,
    compareListCourse
  };
}

export default reducer;
