// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";
import type {
  ApiCoursesListType,
    ConsolidatedCoursesListType,
    ConsolidatedCourseType
} from "./types";
import { Course } from "./model";
import {
  isNotEmpty,
  isCompleteUrl
} from "../../shared/utils";
import { SITE_URL } from "../../config/app";

import missingSquare from "../../assets/missing-square.jpg";

type NormalizedCoursesType = { courses: ConsolidatedCoursesListType };

export type GetCoursesResponse = {
  currentPage: number,
  nextPage: number,
  totalCount: number
} & NormalizedCoursesType;

export class CourseService {
  api: ApiServiceInterface;

  endpoint: string = "/courses";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _transformCourseData({
    imageAnonse,
    courseID: id,
    name: title,
    shortDescription: description,
    length: duration,
    workDuringCourse: eligibleToWork,
    city,
    country,
    ratings,
    price: { price, currencyType }
  }: any): any {
    const image = isNotEmpty(imageAnonse) ? (isCompleteUrl(imageAnonse) ? `${SITE_URL}${imageAnonse}` : `${SITE_URL}/${imageAnonse}`) : "";
    return {
      id,
      title,
      description,
      duration,
      eligibleToWork,
      location: {
        city,
        country
      },
      rating: {
        rate: isNotEmpty(ratings) ? ratings.rate : 0,
        reviews: isNotEmpty(ratings) ? ratings.reviewCount : 0
      },
      price,
      currencyType,
      image
    };
  }

  _transformCompareCourseData({
    imageAnonse,
    intensity,
    courseID: id,
    name: title,
    length,
    workDuringCourse: eligibleToWork,
    city,
    country,
    price: { price, currencyType },
    type
  }: any): any {
    const image = isNotEmpty(imageAnonse) ? (isCompleteUrl(imageAnonse) ? `${SITE_URL}${imageAnonse}` : `${SITE_URL}/${imageAnonse}`) : "";
    return {
      id,
      title,
      duration: length ? length : "-",
      eligibleToWork,
      location: `${city}, ${country}`,
      price,
      image,
      type,
      currencyType,
      intensity
    };
  }

  _normalizeCourse(apiCourse: ApiCoursesListType): ConsolidatedCourseType {
    // $FlowFixMe
    return Course.fromCourseApi(apiCourse);
  }

  _normalizeCourses(apiCourses: any): NormalizedCoursesType {
    const courses = [];

    apiCourses.map(apiCourse => {
      let course = this._transformCourseData(apiCourse._source);
      courses.push(course);
    });
    return {
      courses
    };
  }

  _normalizeCompareCourses(apiCourses: any): NormalizedCoursesType {
    const courses = [];

    apiCourses.map(apiCourse => {
      let course = this._transformCompareCourseData(apiCourse._source);
      courses.push(course);
    });
    return {
      courses
    };
  }

  _normalizeCourseActivity(apiActivity: any, isAvailable: boolean): any {
    // $FlowFixMe
    return {
      name: apiActivity.name,
      image: isAvailable ? `${SITE_URL}${apiActivity.images.x2}` : missingSquare
    };
  }

  _normalizeCourseActivities(apiAcitivities: any): any {
    const activities = [];

    apiAcitivities.activities.map(apiCourse => {
      let activity = this._normalizeCourseActivity(apiCourse, true);
      activities.push(activity);
    });

    apiAcitivities.unavailable.map(apiCourse => {
      let activity = this._normalizeCourseActivity(apiCourse, false);
      activities.push(activity);
    });
    return {
      activities
    };
  }

  _normalizeCourseFacility(apiFacility: any, isAvailable: boolean): any {
    // $FlowFixMe
    return {
      name: apiFacility.name,
      image: isAvailable ? `${SITE_URL}${apiFacility.images.x2}` : missingSquare
    };
  }

  _normalizeCourseFacilities(apiAcitivities: any): any {
    const facilities = [];

    apiAcitivities.facilities.map(apiCourse => {
      let facility = this._normalizeCourseFacility(apiCourse, true);
      facilities.push(facility);
    });

    apiAcitivities.unavailable.map(apiCourse => {
      let facility = this._normalizeCourseFacility(apiCourse, false);
      facilities.push(facility);
    });
    return {
      facilities
    };
  }

  getCourse(courseID: number): Promise<ConsolidatedCourseType> {
    return this.api.get(`${this.endpoint}/fetch/${courseID}`).then(response => {
      return this._normalizeCourse(response.result);
    });
  }

  filterCourses(filters: Object = {}) {
    return this.api.get(`${this.endpoint}/filter`, filters).then(response => {
      return {
        ...this._normalizeCourses(response.result),
        pagination: {
          items: response.total,
          currentPage: filters.page ? filters.page : 1,
          pages: response.pagination.total_pages,
          pageSize: response.pagination.per_page
        }
      };
    });
  }

  getFeaturedCourses() {
    return this.api
      ._fetch("get", `${this.endpoint}/featured`)
      .then(response => {
        return {
          ...this._normalizeCourses(response.result)
        };
      });
  }

  getFilterParams(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}/filterParams`, filters)
      .then(response => {
        return { ...response.array };
      });
  }

  getRelatedCourses(filters: Object = {}) {
    return this.api
      ._fetch("get", `${this.endpoint}/filter`, {}, { ...filters, size: 4 })
      .then(response => {
        return {
          ...this._normalizeCourses(response.result)
        };
      });
  }

  getCourseActivities(filters: Object = {}) {
    return this.api
      .get("/schools/features/activities", filters)
      .then(response => {
        return {
          ...this._normalizeCourseActivities(response.result)
        };
      });
  }

  getCourseFacilities(filters: Object = {}) {
    return this.api
      .get("/schools/features/facilities", filters)
      .then(response => {
        return {
          ...this._normalizeCourseFacilities(response.result)
        };
      });
  }

  addCourseToBookmarkList(payload: Object) {
    return this.api.post("/bookmark/courses", payload);
  }

  removeCourseFromBookmarkList(payload: Object) {
    return this.api.post("/bookmark/delete/courses", payload);
  }

  getBookmarkCourseList() {
    return this.api.get("/bookmark/course").then(results => results.data);
  }

  getBookmarkedCourses(filters: Object = {}) {
    return this.api
      .get(`/bookmark/courses/details`, { ...filters, size: 9 })
      .then(response => {
        return {
          ...this._normalizeCourses(response.data),
          pagination: {
            items: response.total,
            currentPage: filters.page ? filters.page : 1,
            pages: response.pagination.total_pages,
            pageSize: response.pagination.per_page
          }
        };
      });
  }

  getComparedCourseList(payload: any) {
    return this.api
      .post(`/bookmark/compare/open/course`, payload)
      .then(response => {
        return {
          ...this._normalizeCompareCourses(response.compairList)
        };
      });
  }

  getCurrecyList() {
    return this.api.get("/courses/currency/types");
  }

  getCurrecyRates(currency: string) {
    return this.api.get(`/courses/currency/rates/${currency}`);
  }
}
