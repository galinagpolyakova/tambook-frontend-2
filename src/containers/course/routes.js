// import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/course/:courseId",
    className: "course-details-page",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/details"),
      loading: LoadingComponent
    })
  },
  {
    path: "/filter",
    className: "course-filter-page",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/filter"),
      loading: LoadingComponent
    })
  },
  {
    path: "/compare",
    className: "course-filter-page",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/compare"),
      loading: LoadingComponent
    })
  }
];
