// @flow
type apiUserReview = {
  studentName: string,
  reviewText: string,
  rate: number,
  detailPicture: string
};

type userReview = {
  student: string,
  comment: string,
  rating: number,
  image: string
};

type ScheduleType = {
  day: string,
  from: string,
  to: string
};

type DiscountType = {
  weeksMin: number,
  weeksMax: number,
  countryOfOrigin: string,
  discountedPrice: number
};

type DiscountListType = Array<DiscountType>;

type ScheduleListType = Array<ScheduleType>;

export type CoursesListType = Array<CourseType>;

export type CourseFeeType = {
  price: number,
  weeksMax: number,
  weeksMin: number
};

export type CourseFeeListType = Array<CourseFeeType>;

export type CourseType = {
  id: string,
  title: string,
  schoolName: string,
  enrolmentFee: number,
  schoolId: number,
  courseType: string,
  location: {
    city: string,
    country: string
  },
  minAge: number,
  workEligibility: boolean,
  language: string,
  courseFees: CourseFeeListType,
  intensity: number,
  googleMap: string,
  rating: {
    reviews: number,
    rate: number
  },
  images: Array<string>,
  startingPrice: number,
  reviews: Array<userReview>,
  information: Object,
  youtubeLink: string,
  faq: Array<{
    title: string,
    content: any
  }>,
  relatedCources: ConsolidatedCoursesListType,
  homestays: ConsolidatedHomestayListType,
  isBookmarked: boolean,
  schedule: ScheduleListType,
  startDates: Array<string>,
  discountList: DiscountListType,
  isPaymentEnabled: boolean,
  currencyType: string,
  googleCordinates: {
    latitude: string,
    longitude: string
  },
  website: string
};

export type ApiCoursesListType = Array<ApiCourseType>;

export type ApiCourseType = {
  course: {
    imageFull: string,
    schoolId: string,
    minStudentAge: number,
    numberOfStudents: number,
    mnemonicCode: string,
    textAnonse: string,
    reviews: Array<apiUserReview>,
    url: string,
    name: string,
    country: string,
    textFull: string,
    language: Array<string>,
    city: string,
    maxCourseLength: number,
    reviewCount: number,
    active: string,
    minCourseLength: number,
    workEligibility: true,
    studentFeedback: string,
    id: number,
    intensity: number,
    startDates: Array<{ date: string }>,
    location: {
      city: string,
      country: string
    },
    rating: {
      reviews: number,
      rate: number
    },
    courseDiscount: DiscountListType,
    schedule: ScheduleListType,
    coursePrice: Array<{
      weeksMin: number,
      price: number,
      weeksMax: number
    }>,
    courseType: string,
    intensity: number,
    school: number,
    // FIXME: Change to boolean
    isEvening: string,
    schoolDetail: {
      name: string,
      activities: Array<string>,
      shortDescription: string
    },
    imageAnonse: string,
    minEnglishLevel: string,
    startDates_noFormated: string,
    images: Array<{
      url: string,
      iscover: boolean
    }>,
    courseShortName: string,
    priceFrom: string,
    hoursNumber: number,
    courseFilterDetails: {
      selectedLanguge: string
    },
    currencyType: string
  },
  filteredPrice: Object,
  data: {
    id: string,
    title: string,
    schoolName: string,
    location: {
      city: string,
      country: string
    },
    images: Array<{
      url: string,
      iscover: boolean
    }>
  },
  school: {
    videoUrl: string,
    enrolmentFee: number,
    minStudentAge: number,
    rating: number,
    txt_detail: string,
    latitude: string,
    longitude: string,
    courseType: Array<any>,
    placeId: string,
    address: string,
    logo: string,
    url: string,
    country: string,
    name: string,
    language: Array<string>,
    city: string,
    active: string,
    code: string,
    imageAnonse: string,
    images: Array<{
      type: string,
      url: string
    }>,
    id: number,
    accomadation: Array<{
      graterthan18Price: number,
      under18Price: number,
      name: string,
      homestayPlacement: number,
      images: Array<Object>
    }>,
    facilities: Array<string>,
    isPaymentEnabled: boolean,
    contactInfo: {
      WebAddress: string,
      ContactNumber: string,
      Email: string,
      ContactNumber: string,
      prefix: string
    }
  }
};

export type ConsolidatedCoursesListType = Array<ConsolidatedCourseType>;

export type ConsolidatedCourseType = {
  id: string,
  title: string,
  duration: number,
  description: string,
  eligibleToWork: boolean,
  isBookmarked: boolean,
  isAddedToCompare: boolean,
  currencyType: string,
  location: {
    city: string,
    country: string
  },
  rating: {
    rate: number,
    reviews: number
  },
  price: number,
  image: string
};

export type ConsolidatedHomestayListType = Array<ConsolidatedHomestayType>;

export type ConsolidatedHomestayType = {
  above18Price: number,
  under18Price: number,
  homestayFee: number,
  title: string,
  image: string
};
