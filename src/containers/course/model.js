// @flow
import type { CourseType, ApiCourseType } from "./types";
import type { SerializableInterface } from "../../shared/SerializableInterface";
import missingSquare from "../../assets/missing-square.jpg";
import { SITE_URL } from "../../config/app";
import { isNotEmpty } from "../../shared/utils";
import missingImage from "../../assets/missing-profile-picture.png";

export class Course implements SerializableInterface<CourseType> {
  id: $PropertyType<CourseType, "id">;
  title: $PropertyType<CourseType, "title">;
  courseType: $PropertyType<CourseType, "courseType">;
  schoolId: $PropertyType<CourseType, "schoolId">;
  schoolName: $PropertyType<CourseType, "schoolName">;
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">;
  location: $PropertyType<CourseType, "location">;
  minAge: $PropertyType<CourseType, "minAge">;
  workEligibility: $PropertyType<CourseType, "workEligibility">;
  language: $PropertyType<CourseType, "language">;
  courseFees: $PropertyType<CourseType, "courseFees">;
  googleMap: $PropertyType<CourseType, "googleMap">;
  rating: $PropertyType<CourseType, "rating">;
  images: $PropertyType<CourseType, "images">;
  startingPrice: $PropertyType<CourseType, "startingPrice">;
  reviews: $PropertyType<CourseType, "reviews">;
  information: $PropertyType<CourseType, "information">;
  youtubeLink: $PropertyType<CourseType, "youtubeLink">;
  faq: $PropertyType<CourseType, "faq">;
  relatedCources: $PropertyType<CourseType, "relatedCources">;
  homestays: $PropertyType<CourseType, "homestays">;
  intensity: $PropertyType<CourseType, "intensity">;
  isBookmarked: $PropertyType<CourseType, "isBookmarked">;
  schedule: $PropertyType<CourseType, "schedule">;
  startDates: $PropertyType<CourseType, "startDates">;
  discountList: $PropertyType<CourseType, "discountList">;
  isPaymentEnabled: $PropertyType<CourseType, "isPaymentEnabled">;
  currencyType: $PropertyType<CourseType, "currencyType">;
  googleCordinates: $PropertyType<CourseType, "googleCordinates">;
  website: $PropertyType<CourseType, "website">;

  constructor({
    data: { id, title, schoolName, location, images },
    school: {
      enrolmentFee,
      address,
      facilities,
      videoUrl,
      isPaymentEnabled,
      accomadation,
      latitude,
      longitude,
      contactInfo: { WebAddress }
    },
    course: {
      school,
      courseType,
      courseDiscount,
      schedule,
      minStudentAge,
      workEligibility,
      startDates,
      language,
      coursePrice,
      rating,
      reviews,
      textFull,
      schoolDetail,
      currencyType,
      intensity
    }
  }: ApiCourseType) {
    this.id = id;
    this.title = title;
    this.schoolId = school;
    this.courseType = courseType;
    this.schoolName = schoolName;
    this.enrolmentFee = enrolmentFee;
    this.discountList = courseDiscount;
    this.location = location;
    this.schedule = schedule;
    this.minAge = minStudentAge;
    this.workEligibility = workEligibility;
    this.startDates = startDates.map(({ date }) => date);
    this.language = language.join();
    this.courseFees = coursePrice;
    this.googleMap = address;
    this.website = WebAddress;
    this.intensity = intensity;
    this.googleCordinates = {
      latitude,
      longitude
    };
    this.rating = {
      reviews: rating.reviews,
      rate: rating.rate
    };
    this.startingPrice = coursePrice[0].price;
    this.images = images.map(image => `${SITE_URL}${image.url}`);
    this.reviews = reviews.map(review => ({
      student: review.studentName,
      comment: review.reviewText,
      rating: review.rate,
      image: isNotEmpty(review.detailPicture) ? `${SITE_URL}${review.detailPicture}` : missingImage
    }));

    this.relatedCources = [];

    this.information = {
      description: textFull,
      activities: schoolDetail.activities,
      facilities: facilities
    };

    this.youtubeLink = videoUrl;
    this.homestays = accomadation.map(accomadation => ({
      above18Price: accomadation.graterthan18Price,
      under18Price: accomadation.under18Price,
      title: accomadation.name,
      image:
        accomadation.images && accomadation.images[0]
          ? accomadation.images[0].url
          : missingSquare,
      homestayFee: accomadation.homestayPlacement
    }));
    this.isPaymentEnabled = isPaymentEnabled;
    this.currencyType = currencyType;
  }

  static fromCourseApi(apiCourse: ApiCourseType) {
    return new this(apiCourse);
  }

  toJSON(): CourseType {
    return this;
  }
}
