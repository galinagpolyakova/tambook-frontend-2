// @flow
import type { ApplicationState } from "../../../store/reducers";

export const isAuthenticated = (state: ApplicationState) =>
  state.auth.isAuthenticated;

export const isUserInitiated = (state: ApplicationState) =>
  state.auth.isUserInitiated;
