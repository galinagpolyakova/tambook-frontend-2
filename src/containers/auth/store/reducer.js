// @flow
import type { Action } from "../../../shared/types/ReducerAction";

import {
  AUTH_INIT,
  AUTH_SIGNIN_SUCCESS,
  AUTH_FAILURE,
  AUTH_SIGNUP_SUCCESS,
  AUTH_SIGNOUT_SUCCESS,
  IS_USER_AUTHENTICATED_SUCCUSS,
  IS_USER_AUTHENTICATED_FAILURE,
  CLEAR_NOTIFICATIONS,
  AUTH_FORGOT_PASSWORD_SUCCESS,
  AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
  AUTH_CONFIRM_SIGN_UP_SUCCESS
} from "./actionTypes";

export type AuthState = {
  loading: boolean,
  isAuthenticated: boolean,
  isUserInitiated: boolean,
  isAuthSuccess: boolean,
  error: null | string,
  email: string,
  isForgotPasswordSubmitted: boolean,
  isEmailSent: boolean
};

const intialState: AuthState = {
  loading: false,
  isAuthenticated: false,
  isUserInitiated: false,
  isAuthSuccess: false,
  error: null,
  email: "",
  isForgotPasswordSubmitted: false,
  isEmailSent: false
};

function authInit(state: AuthState): AuthState {
  return {
    ...state,
    loading: true,
    error: null,
    isAuthSuccess: false
  };
}

function authSigninSuccess(state: AuthState, { email }): AuthState {
  return {
    ...state,
    loading: false,
    isAuthenticated: true,
    isAuthSuccess: true,
    email
  };
}

function authSignupSuccess(state: AuthState): AuthState {
  return {
    ...state,
    loading: false,
    isAuthSuccess: true,
    isEmailSent: true
  };
}

function authFailure(state: AuthState, { error }): AuthState {
  return {
    ...state,
    error,
    loading: false
  };
}

function isUserAuthenticatedSuccess(state: AuthState, { email }): AuthState {
  return {
    ...state,
    isAuthenticated: true,
    isUserInitiated: true,
    isAuthSuccess: true,
    loading: false,
    email
  };
}

function authSignOutSuccess(state: AuthState): AuthState {
  return {
    ...state,
    isAuthenticated: false,
    isAuthSuccess: false,
    loading: false
  };
}

function isUserAuthenticatedFailure(state: AuthState): AuthState {
  return {
    ...state,
    isUserInitiated: true,
    loading: false
  };
}

function clearNotifications(state: AuthState): AuthState {
  return {
    ...state,
    error: null,
    isEmailSent: false
  };
}

function authForgotPasswordSuccess(state: AuthState): AuthState {
  return {
    ...state,
    isForgotPasswordSubmitted: true
  };
}

function authForgotPasswordSubmitSuccess(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true
  };
}

function authConfirmSignUpSuccess(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true,
    loading: false,
    isEmailSent: false
  };
}

const reducer = (
  state: AuthState = intialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case AUTH_INIT:
      return authInit(state);
    case AUTH_SIGNIN_SUCCESS:
      return authSigninSuccess(state, payload);
    case AUTH_SIGNUP_SUCCESS:
      return authSignupSuccess(state);
    case AUTH_FAILURE:
      return authFailure(state, payload);
    case IS_USER_AUTHENTICATED_SUCCUSS:
      return isUserAuthenticatedSuccess(state, payload);
    case IS_USER_AUTHENTICATED_FAILURE:
      return isUserAuthenticatedFailure(state);
    case AUTH_SIGNOUT_SUCCESS:
      return authSignOutSuccess(state);
    case CLEAR_NOTIFICATIONS:
      return clearNotifications(state);
    case AUTH_FORGOT_PASSWORD_SUCCESS:
      return authForgotPasswordSuccess(state);
    case AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS:
      return authForgotPasswordSubmitSuccess(state);
    case AUTH_CONFIRM_SIGN_UP_SUCCESS:
      return authConfirmSignUpSuccess(state);
    default:
      return state;
  }
};

export default reducer;
