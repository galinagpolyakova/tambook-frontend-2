// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import {
  authSignIn,
  authSignUp,
  clearNotifications,
  federatedSignInSuccess,
  authForgotPassword,
  authForgotPasswordSubmit
} from "../../store/action";

import { AUTH_TYPE } from "../../constants";

import authImage from "../../../../assets/auckland.jpg";
import Modal from "../../../../shared/components/Modal";
import Alert from "../../../../shared/components/Alert";
import SignIn from "../../components/SignIn";
import SignUp from "../../components/SignUp";
import ForgotPassword from "../../components/ForgotPassword";

import { UserSettingsConsumer } from "../../../../contexts/UserSettings";

import "./styles.css";
import { isString } from "../../../../shared/utils";

const EVENT_KEY_UP = "keyup";

type AuthProps = {
  error: Object | null,
  loading: boolean,
  isAuthSuccess: boolean,
  isEmailSent: boolean,
  isForgotPasswordSubmitted: boolean,
  authSignUp: Function,
  authSignIn: Function,
  toggleAuth: Function,
  clearNotifications: Function,
  federatedSignInSuccess: Function,
  authForgotPassword: Function,
  authForgotPasswordSubmit: Function
};

type AuthState = {
  authState: | typeof AUTH_TYPE.SIGNIN
  | typeof AUTH_TYPE.SIGNUP
  | typeof AUTH_TYPE.FORGOT_PASSWORD,
  isLoading: boolean,
  error: string | null,
  isCapsLockActive: boolean
};

class AuthBlock extends PureComponent<AuthProps, AuthState> {
  state = {
    authState: AUTH_TYPE.SIGNIN,
    isLoading: false,
    error: null,
    isCapsLockActive: false
  };

  componentDidMount() {
    document.addEventListener(EVENT_KEY_UP, this._wasCapsLockDeactivated);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isAuthSuccess !== prevProps.isAuthSuccess &&
      this.props.isAuthSuccess
    ) {
      prevState.authState === AUTH_TYPE.SIGNUP ||
        prevState.authState === AUTH_TYPE.FORGOT_PASSWORD
        ? this.setState({ authState: AUTH_TYPE.SIGNIN })
        : this.props.toggleAuth();
    }
  }

  componentWillUnmount() {
    document.removeEventListener(EVENT_KEY_UP, this._wasCapsLockDeactivated);
  }

  _wasCapsLockDeactivated = (event: any) => {
    if (event.getModifierState) {
      this.setState({
        isCapsLockActive:
          event.getModifierState("CapsLock") &&
          this.state.isCapsLockActive === false
      });
    }
  };

  _toggleAuthType = authState => {
    if (authState !== undefined) {
      if (this.props.error !== null || this.props.isEmailSent) {
        this.props.clearNotifications();
      }
      this.setState({
        authState
      });
    }
  };

  _handleAuthStateChange = state => {
    if (state === "signedIn") {
      this.props.federatedSignInSuccess();
    }
  };

  _getFormFields = () => {
    const { authState, isLoading, isCapsLockActive } = this.state;
    const { loading } = this.props;

    switch (authState) {
      case AUTH_TYPE.SIGNUP:
        return (
          <SignUp
            toggleAuthType={this._toggleAuthType}
            isLoading={loading}
            onFormSubmit={this.props.authSignUp}
            notifications={this._getNotifications()}
            isCapsLockActive={isCapsLockActive}
          />
        );
      case AUTH_TYPE.FORGOT_PASSWORD:
        return (
          <ForgotPassword
            toggleAuthType={this._toggleAuthType}
            forgotPassword={this.props.authForgotPassword}
            forgotPasswordSubmit={this.props.authForgotPasswordSubmit}
            isSubmitted={this.props.isForgotPasswordSubmitted}
            notifications={this._getNotifications()}
            isLoading={isLoading}
            isCapsLockActive={isCapsLockActive}
          />
        );
      default:
        return (
          <SignIn
            toggleAuthType={this._toggleAuthType}
            isLoading={loading}
            onFormSubmit={this.props.authSignIn}
            notifications={this._getNotifications()}
            isCapsLockActive={isCapsLockActive}
            handleAuthStateChange={this._handleAuthStateChange}
          />
        );
    }
  };

  _getNotifications = () => {
    const { error, isEmailSent } = this.props;
    if (error !== null) {
      if (isString(error)) {
        return {
          message: <Alert type={Alert.type.ERROR}>{error}</Alert>,
          code: error.code
        };
      } else {
        if (error.code === "UserNotFoundException") {
          return {
            message: (
              <Alert type={Alert.type.ERROR}>The user does not exist.</Alert>
            ),
            code: error.code
          };
        }
        return {
          message: <Alert type={Alert.type.ERROR}>{error.message}</Alert>,
          code: error.code
        };
      }
    }
    if (isEmailSent) {
      return {
        message: (
          <Alert type={Alert.type.SUCCSESS}>Check Your Email.</Alert>
        ),
        code: null
      };
    }
    return null;
  };

  render() {
    const { toggleAuth } = this.props;
    return (
      <Modal className="auth-modal" showModal={true} onClose={toggleAuth}>
        <div className="auth-container">
          <div className="auth-content">
            <div className="auth-body">
              <div className="form-header">
                <div className="heading">WELCOME TO TAMBOOK!</div>
              </div>

              <div className="sub-heading">
                {this.state.authState === AUTH_TYPE.FORGOT_PASSWORD
                  ? "Reset your password"
                  : "Please login or create an account"}
              </div>
              {this._getFormFields()}

              <div className="footer-links">
                <a target="_new" href="/under-construction">
                  PRIVACY POLICY
                </a>
                <a target="_new" href="/under-construction">
                  TERMS AND CONDITIONS
                </a>
              </div>
            </div>
          </div>
          <div
            className="auth-image"
            style={{
              background: `linear-gradient(
                      rgba(0, 160, 186, 0.43),
                      rgba(0, 160, 186, 0.43)
                      )
                      , center / cover no-repeat url(${authImage})`
            }}
          />
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    error: state.auth.error,
    loading: state.auth.loading,
    isAuthSuccess: state.auth.isAuthSuccess,
    isForgotPasswordSubmitted: state.auth.isForgotPasswordSubmitted,
    isEmailSent: state.auth.isEmailSent
  };
}

const Actions = {
  authSignIn,
  authSignUp,
  clearNotifications,
  federatedSignInSuccess,
  authForgotPassword,
  authForgotPasswordSubmit
};

const Auth = connect(
  mapStateToProps,
  Actions
)(AuthBlock);

export default class AuthContext extends PureComponent<{}> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ showAuth, toggleAuth }) => {
          return showAuth && <Auth {...this.props} toggleAuth={toggleAuth} />;
        }}
      </UserSettingsConsumer>
    );
  }
}
