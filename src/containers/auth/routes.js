// import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/confirm",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/confirm"),
      loading: LoadingComponent
    })
  }
];
