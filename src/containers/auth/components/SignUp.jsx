// @flow
import React, { Component, Fragment } from "react";
import Input from "../../../shared/components/Input";
import Button from "../../../shared/components/Button";
import Checkbox from "../../../shared/components/Checkbox";
import Icon from "../../../shared/components/Icon";

import { isEmail, isName } from "../../../shared/kernel/cast";
import { AUTH_TYPE } from "../constants";
import { hasWhiteSpace } from "../../../shared/utils";

type SignUpProps = {
  onFormSubmit: Function,
  toggleAuthType: Function,
  isLoading: boolean,
  notifications: null | Object,
  isCapsLockActive: boolean
};

type SignUpState = {
  values: {
    email: string,
    name: string,
    password: string
  },
  errors: {
    email: null | string,
    name: null | string,
    password: null | string
  }
};

class SignUp extends Component<SignUpProps, SignUpState> {
  state = {
    values: {
      email: "",
      name: "",
      password: ""
    },
    errors: {
      email: null,
      name: null,
      password: null
    }
  };

  _resetForm = () => {
    this.setState({
      errors: {
        email: null,
        name: null,
        password: null,
        confirmPassword: null
      }
    });
  };

  _validateForm = () => {
    const { name, email, password } = this.state.values;

    let hasError = false;

    this._resetForm();

    if (email === "") {
      this._setFormErrors("email", "Email is required.");
      hasError = true;
    } else if (hasWhiteSpace(email)) {
      this._setFormErrors("email", "Email connot contain spaces.");
      hasError = true;
    } else if (!isEmail(email)) {
      this._setFormErrors("email", "Email is invalid.");
      hasError = true;
    }

    if (name === "") {
      this._setFormErrors("name", "Name is required.");
      hasError = true;
    } else if (!isName(name)) {
      this._setFormErrors("name", "Name is invalid.");
      hasError = true;
    }

    if (password === "") {
      this._setFormErrors("password", "Password is required.");
      hasError = true;
    } else if (
      !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/.test(
        password
      )
    ) {
      this._setFormErrors(
        "password",
        "Password must contain at least 8 characters including a number, a special character, lower case and upper case letters."
      );
      hasError = true;
    } else if (hasWhiteSpace(password)) {
      this._setFormErrors("password", "Password connot contain spaces.");
      hasError = true;
    }
    return hasError;
  };

  _handleInputChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      values: {
        ...this.state.values,
        [event.target.id]: event.target.value
      }
    });
  };

  _handleSignupSubmit = () => {
    if (!this._validateForm()) {
      const { email, name, password } = this.state.values;
      this.props.onFormSubmit({ email: email.trim(), name, password });
    }
  };

  _setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        errors: {
          ...state.errors,
          [field]: message
        }
      };
    });
  };

  _getNotifications = () => {
    if (this.props.notifications !== null) {
      return this.props.notifications.message;
    }
    return null;
  };

  render() {
    return (
      <Fragment>
        <form autoComplete="off">
          <div className="form-group">
            <label>Email</label>
            <Input
              placeholder="Eg: user@example.com"
              id="email"
              onChange={this._handleInputChange}
              error={this.state.errors.email}
              autoComplete={false}
            />
          </div>
          <div className="form-group">
            <label>Name</label>
            <Input
              placeholder="Full name"
              id="name"
              onChange={this._handleInputChange}
              error={this.state.errors.name}
              autoComplete={false}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <Input
              placeholder="at least 6 characters including a number, a special character, lower case and upper case letters."
              id="password"
              onChange={this._handleInputChange}
              type="password"
              error={this.state.errors.password}
              autoComplete={false}
            />
            {this.props.isCapsLockActive && (
              <span className="warning">
                <Icon icon="exclamation-circle" /> Warning: Caps lock enabled.
              </span>
            )}
          </div>
          <Checkbox
            text="Register now"
            isChecked={true}
            onChange={() => this.props.toggleAuthType(AUTH_TYPE.SIGNIN)}
          />
        </form>
        {this._getNotifications()}
        <div className="button-container">
          <Fragment>
            <Button
              htmlType={Button.htmlType.SUBMIT}
              onClick={() => this._handleSignupSubmit()}
              loading={this.props.isLoading}
            >
              SIGN UP AS STUDENT
            </Button>
          </Fragment>
        </div>
      </Fragment>
    );
  }
}

export default SignUp;
