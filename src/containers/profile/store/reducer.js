// @flow
import { type Action } from "../../../shared/types/ReducerAction";
import { type PaginationType } from "../../../shared/components/Pagination";
import { saveData, loadData } from "../../../shared/kernel/localStorage";

import {
  GET_BOOKMARKED_COURSES_SUCCESS,
  REMOVE_BOOKMARKED_COURSE_SUCCESS,
  GET_USER_PROFILE_SUCCESS,
  GET_BOOKINGS_SUCCESS,
  GET_CURRENCY_LIST_SUCCESS,
  SAVE_USER_PREFERENCES,
  GET_CURRENCY_RATE_SUCCESS,
  GET_USER_PREFERENCES
} from "./actionTypes";

export type ProfileState = {
  bookmarkedCourses: {
    pagination: PaginationType,
    courses: Array<any>
  },
  bookings: Array<Object>,
  userProfile: {
    name: string,
    email: string,
    gender: string,
    birthDate: string,
    citizenship: string,
    language: string
  } | null,
  shouldCoursesLoad: boolean,
  currencies: null | Object,
  rates: null | Object,
  settings: {
    language: string,
    passport: string,
    currency: string
  }
};

const intialState: ProfileState = {
  bookmarkedCourses: {
    pagination: null,
    courses: []
  },
  bookings: [],
  userProfile: null,
  shouldCoursesLoad: false,
  currencies: null,
  rates: null,
  settings: {
    language: "en_US",
    passport: "US",
    currency: "USD"
  }
};

const reducer = (
  state: ProfileState = intialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_BOOKMARKED_COURSES_SUCCESS:
      return getBookmarkedCoursesSuccess(state, payload);
    case REMOVE_BOOKMARKED_COURSE_SUCCESS:
      return removeBookmarkedCourseSuccess(state);
    case GET_USER_PROFILE_SUCCESS:
      return getUserProfileSuccess(state, payload);
    case GET_BOOKINGS_SUCCESS:
      return getBookingsSuccess(state, payload);
    case GET_CURRENCY_LIST_SUCCESS:
      return getCurrencyListSuccess(state, payload);
    case GET_USER_PREFERENCES:
      return getUserPreferences(state);
    case SAVE_USER_PREFERENCES:
      return saveUserPreferences(state, payload);
    case GET_CURRENCY_RATE_SUCCESS:
      return getCurrencyRateSuccess(state, payload);
    default:
      return state;
  }
};

function getBookmarkedCoursesSuccess(
  state: ProfileState,
  { courses, pagination }
) {
  return {
    ...state,
    bookmarkedCourses: {
      courses,
      pagination
    },
    shouldCoursesLoad: false
  };
}

function removeBookmarkedCourseSuccess(state: ProfileState) {
  return {
    ...state,
    shouldCoursesLoad: true,
    bookmarkedCourses: {
      pagination: null,
      courses: []
    }
  };
}

function getUserProfileSuccess(state: ProfileState, userProfile) {
  return {
    ...state,
    userProfile
  };
}

function getBookingsSuccess(state: ProfileState, { bookings }) {
  return {
    ...state,
    bookings
  };
}

function getCurrencyListSuccess(state, { currencies }) {
  return {
    ...state,
    currencies
  };
}

function getUserPreferences(state) {
  const settings =
    loadData("userSettings") !== undefined
      ? loadData("userSettings")
      : state.settings;
  return {
    ...state,
    settings
  };
}

function saveUserPreferences(state, settings) {
  saveData("userSettings", settings);
  return {
    ...state,
    settings
  };
}

function getCurrencyRateSuccess(state, { rates }) {
  return {
    ...state,
    rates
  };
}

export default reducer;
