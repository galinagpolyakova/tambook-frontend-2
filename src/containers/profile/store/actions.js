import {
  PROFILE_ASYNC_INIT,
  GET_BOOKMARKED_COURSES_SUCCESS,
  GET_BOOKMARKED_COURSES_FAILURE,
  REMOVE_BOOKMARKED_COURSE_SUCCESS,
  GET_USER_PROFILE_SUCCESS,
  GET_BOOKINGS_SUCCESS,
  GET_CURRENCY_LIST_SUCCESS,
  SAVE_USER_PREFERENCES,
  GET_CURRENCY_RATE_SUCCESS,
  GET_USER_PREFERENCES
} from "./actionTypes";

import { Auth } from "aws-amplify";

export function profileAsyncInit() {
  return {
    type: PROFILE_ASYNC_INIT
  };
}

function getBookmarkedCoursesSuccess(payload) {
  return {
    type: GET_BOOKMARKED_COURSES_SUCCESS,
    payload
  };
}

function getBookmarkedCoursesFailure() {
  return {
    type: GET_BOOKMARKED_COURSES_FAILURE
  };
}

export function getBookmarkedCourses(filter) {
  return (dispatch, getState, serviceManager) => {
    dispatch(profileAsyncInit());

    let courseService = serviceManager.get("CourseService");
    courseService
      .getBookmarkedCourses(filter)
      .then(({ courses, pagination }) =>
        dispatch(
          getBookmarkedCoursesSuccess({
            courses,
            pagination
          })
        )
      )
      .catch(err => {
        dispatch(getBookmarkedCoursesFailure(err));
      });
  };
}

function removeBookmarkedCourseSuccess(payload) {
  return {
    type: REMOVE_BOOKMARKED_COURSE_SUCCESS,
    payload
  };
}

export function removeBookmarkedCourse(courseId) {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");
    let { email } = getState().auth;

    courseService
      .removeCourseFromBookmarkList({ email, list: [courseId] })
      .then(() => dispatch(removeBookmarkedCourseSuccess({ id: courseId })));
  };
}

function getUserProfileSuccess(payload) {
  return {
    type: GET_USER_PROFILE_SUCCESS,
    payload
  };
}

export function getUserProfile() {
  return dispatch => {
    Auth.currentAuthenticatedUser().then(
      ({
        attributes: {
          name,
          email,
          "custom:gender": gender,
          "custom:birth_date": birth_date,
          "custom:citizenship": citizenship,
          "custom:phone": phone,
          "custom:language": language
        }
      }) => {
        dispatch(
          getUserProfileSuccess({
            name,
            email,
            gender: gender ? gender : "",
            phone: phone ? phone : "",
            birth_date,
            citizenship: citizenship ? citizenship : "",
            language: language ? language : ""
          })
        );
      }
    );
  };
}

export function updateProfile({
  phone,
  gender,
  citizenship,
  language,
  birth_date,
  name
}) {
  return () => {
    Auth.currentAuthenticatedUser().then(user => {
      Auth.updateUserAttributes(user, {
        "custom:phone": phone,
        "custom:gender": gender,
        "custom:citizenship": citizenship,
        "custom:language": language,
        "custom:birth_date": birth_date,
        name
      });
    });
  };
}

export function getBookings() {
  return (dispatch, getState, serviceManager) => {
    let bookingService = serviceManager.get("BookingService");

    bookingService
      .getBookings()
      .then(payload => dispatch(getBookingsSuccess(payload)));
  };
}

function getBookingsSuccess(payload) {
  return {
    type: GET_BOOKINGS_SUCCESS,
    payload
  };
}

function getCurrencyListSuccess(payload) {
  return {
    type: GET_CURRENCY_LIST_SUCCESS,
    payload
  };
}

export function getCurrencyList() {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");

    courseService
      .getCurrecyList()
      .then(({ data: currencies }) =>
        dispatch(getCurrencyListSuccess({ currencies }))
      );
  };
}

export function getUserPreferences() {
  return {
    type: GET_USER_PREFERENCES
  };
}

export function saveUserPreferences(payload) {
  return {
    type: SAVE_USER_PREFERENCES,
    payload
  };
}

function getCurrencyRateSuccess(payload) {
  return {
    type: GET_CURRENCY_RATE_SUCCESS,
    payload
  };
}

export function getCurrencyRate(currency) {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");

    courseService
      .getCurrecyRates(currency)
      .then(({ data: { rates } }) =>
        dispatch(getCurrencyRateSuccess({ rates }))
      );
  };
}
