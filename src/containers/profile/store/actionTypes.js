const PREFIX = "@profile/";

export const PROFILE_ASYNC_INIT = `${PREFIX}PROFILE_ASYNC_INIT`;

export const GET_BOOKMARKED_COURSES_SUCCESS = `${PREFIX}GET_BOOKMARKED_COURSES_SUCCESS`;
export const GET_BOOKMARKED_COURSES_FAILURE = `${PREFIX}GET_BOOKMARKED_COURSES_FAILURE`;

export const REMOVE_BOOKMARKED_COURSE_SUCCESS = `${PREFIX}REMOVE_BOOKMARKED_COURSE_SUCCESS`;

export const GET_USER_PROFILE_SUCCESS = `${PREFIX}GET_USER_PROFILE_SUCCESS`;

export const GET_BOOKINGS_SUCCESS = `${PREFIX}GET_BOOKINGS_SUCCESS`;

export const GET_CURRENCY_LIST_SUCCESS = `${PREFIX}GET_CURRENCY_LIST_SUCCESS`;

export const SAVE_USER_PREFERENCES = `${PREFIX}SAVE_USER_PREFERENCES`;

export const GET_CURRENCY_RATE_SUCCESS = `${PREFIX}GET_CURRENCY_RATE_SUCCESS`;

export const GET_USER_PREFERENCES = `${PREFIX}GET_USER_PREFERENCES`;
