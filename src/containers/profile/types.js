// @flow
export type Booking = {
  id: string,
  title: string,
  duration: number,
  date: string,
  status: string,
  total: number
};
export type BookingList = Array<Booking>;
