import React, { Component } from "react";
import Section from "../../../../shared/components/Section";

class Booking extends Component {
  render() {
    return (
      <div className="container">
        <Section>
          <div className="booking-summary">
            <div className="heading">Course information</div>
            <div className="summary-block">
              <div className="summary-line">
                <div className="title">Name</div>
                <div className="content">Course</div>
              </div>
            </div>
          </div>
        </Section>
      </div>
    );
  }
}

export default Booking;
