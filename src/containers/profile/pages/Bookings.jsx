// @flow
import React, { Component } from "react";
import CourseBooking from "../components/CourseBooking";
import Tabs from "../../../shared/components/Tabs";
import Layout from "../components/layout";

import { getBookings } from "../store/actions";

import { connect } from "react-redux";

type BookingsProps = {
  getBookings: Function,
  bookings: any[]
};

class Bookings extends Component<BookingsProps> {
  componentDidMount() {
    this.props.getBookings();
  }
  render() {
    return (
      <Layout currentLink="bookings" currentTitle="Bookings">
        <div className="bookings">
          <Tabs
            items={[
              {
                title: "Active",
                content: <CourseBooking bookings={this.props.bookings} />
              }
            ]}
          />
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return { bookings: state.profile.bookings };
}

const Actions = {
  getBookings
};

export default connect(
  mapStateToProps,
  Actions
)(Bookings);
