// @flow
import { type ConsolidatedCoursesListType } from "../../course/types";
import { type PaginationType } from "../../../shared/components/Pagination";

import React, { Component } from "react";
import { connect } from "react-redux";

import { getBookmarkedCourses, removeBookmarkedCourse } from "../store/actions";

import {
  addCourseToCompare,
  removeCourseFromCompare
} from "../../course/store/actions";

import { isAuthenticated } from "../../auth/store/selectors";
import {
  getConsolidatedCourseBookmarkList,
  isCourseBookmarksListLoaded
} from "../store/selectors";

import Layout from "../components/layout";
import Tabs from "../../../shared/components/Tabs";
import CourseView from "../../course/components/CourseView";

type BookmarksProps = {
  isAuthenticated: boolean,
  isBookmarksListLoaded: boolean,
  getBookmarkedCourses: Function,
  removeBookmarkedCourse: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function,
  courses: ConsolidatedCoursesListType,
  shouldCoursesLoad: boolean,
  pagination: PaginationType
};

class Bookmarks extends Component<BookmarksProps> {
  componentDidMount() {
    if (this.props.isAuthenticated && !this.props.isBookmarksListLoaded) {
      this.props.getBookmarkedCourses({ page: 1 });
    }
  }

  componentDidUpdate() {
    if (this.props.shouldCoursesLoad) {
      this.props.getBookmarkedCourses({ page: 1 });
    }
  }

  onPageChanged(filters) {
    this.props.getBookmarkedCourses(filters);
  }

  render() {
    const {
      courses,
      addCourseToCompare,
      removeCourseFromCompare,
      pagination
    } = this.props;
    return (
      <Layout currentLink="bookmarks" currentTitle="Bookmarks">
        <Tabs
          items={[
            {
              title: "Cources",
              content: (
                <CourseView
                  courses={courses}
                  addCourseToBookmarkList={() => {}}
                  removeCourseFromBookmarkList={
                    this.props.removeBookmarkedCourse
                  }
                  addCourseToCompare={addCourseToCompare}
                  removeCourseFromCompare={removeCourseFromCompare}
                  filter={() => {}}
                  pagination={pagination}
                  onPageChanged={this.onPageChanged.bind(this)}
                />
              )
            }
          ]}
        />
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    courses: getConsolidatedCourseBookmarkList(state),
    isAuthenticated: isAuthenticated(state),
    isBookmarksListLoaded: isCourseBookmarksListLoaded(state),
    shouldCoursesLoad: state.profile.shouldCoursesLoad,
    pagination: state.profile.bookmarkedCourses.pagination
  };
}

const Actions = {
  getBookmarkedCourses,
  removeBookmarkedCourse,
  addCourseToCompare,
  removeCourseFromCompare
};

export default connect(
  mapStateToProps,
  Actions
)(Bookmarks);
