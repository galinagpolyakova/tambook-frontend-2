// @flow
import React, { Component } from "react";
import { Storage } from "aws-amplify";

import Row from "../../../shared/components/Row";
import Col from "../../../shared/components/Col";
import Button from "../../../shared/components/Button";
import Input from "../../../shared/components/Input";
import Select from "../../../shared/components/Select";

import * as countries from "../../../shared/data/countries.json";
import * as locales from "../../../shared/data/locales.json";

type EditProfileProps = {
  name: string,
  email: string,
  phone: string,
  gender: string,
  birth_date: string,
  citizenship: string,
  language: string,
  isOnEdit: boolean,
  toggleEdit: Function,
  onCompleteEditing: Function
};

type EditProfileState = {
  name: string,
  email: string,
  phone: string,
  gender: string,
  birth_date: string,
  citizenship: string,
  language: string,
  uploading: boolean
};

class EditProfile extends Component<EditProfileProps, EditProfileState> {
  handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  constructor(props: EditProfileProps) {
    super(props);

    const {
      name,
      email,
      phone,
      gender,
      birth_date,
      citizenship,
      language
    } = this.props;

    this.state = {
      name,
      email,
      phone,
      gender,
      birth_date,
      citizenship,
      language,
      uploading: false
    };
  }

  onChange = async (e: any) => {
    const file = e.target.files[0];
    const fileName = "uuid()";
    this.setState({ uploading: true });
    await Storage.put(fileName, file, {
      customPrefix: { public: "uploads/profile/" },
      metadata: { email: this.props.email }
    });
    this.setState({ uploading: false });
  };

  render() {
    const { isOnEdit } = this.props;
    const {
      name,
      email,
      phone,
      gender,
      birth_date,
      citizenship,
      language
    } = this.state;
    const countryList = [];
    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }
    const localeList = [];
    for (const key in locales) {
      if (locales.hasOwnProperty(key)) {
        localeList.push({
          value: key,
          name: locales[key]
        });
      }
    }
    return (
      <Row>
        {/* {!isOnEdit && (
          <Col size={4}>
            <img src={profileImage} />
          </Col>
        )} */}
        <Col size={6} className="user-details">
          {isOnEdit ? (
            <Row>
              <Col>Name</Col>
              <Col>
                <Input
                  text={name}
                  onChange={this.handleInputChange.bind(this)}
                  id="name"
                />
              </Col>
            </Row>
          ) : (
            <Row>
              <Col>
                <div className="name">{name}</div>
              </Col>
            </Row>
          )}
          <Row>
            <Col>Email</Col>
            <Col>{email}</Col>
          </Row>
          <Row>
            <Col>Phone</Col>
            {isOnEdit ? (
              <Col>
                <Input
                  text={phone}
                  onChange={this.handleInputChange.bind(this)}
                  id="phone"
                />
              </Col>
            ) : (
              <Col>{phone}</Col>
            )}
          </Row>
          <Row>
            <Col>Gender</Col>
            {isOnEdit ? (
              <Col>
                <Select
                  options={[
                    {
                      name: "Male",
                      value: "male"
                    },
                    {
                      name: "Female",
                      value: "female"
                    }
                  ]}
                  id="gender"
                  placeholder="Select the gender"
                  onChange={gender => this.setState({ gender })}
                  selected={gender}
                />
              </Col>
            ) : (
              <Col className="text-capitalize">{gender}</Col>
            )}
          </Row>
          <Row>
            <Col>Birth date</Col>
            {isOnEdit ? (
              <Col className="date-of-birth">
                <Input
                  placeholder="dd/mm/yyyy"
                  id="birth_date"
                  text={birth_date}
                  onChange={this.handleInputChange.bind(this)}
                />
              </Col>
            ) : (
              <Col>{birth_date}</Col>
            )}
          </Row>
          <Row>
            <Col>Citizenship</Col>
            {isOnEdit ? (
              <Col>
                <Select
                  options={countryList}
                  placeholder="Select country"
                  onChange={citizenship => this.setState({ citizenship })}
                  selected={citizenship}
                  searchable
                />
              </Col>
            ) : (
              <Col>{countries[citizenship]}</Col>
            )}
          </Row>
          <Row>
            <Col>Native language</Col>
            {isOnEdit ? (
              <Col>
                <Select
                  options={localeList}
                  placeholder="Select language"
                  onChange={language => this.setState({ language })}
                  selected={language}
                  searchable
                />
              </Col>
            ) : (
              <Col>{locales[language]}</Col>
            )}
          </Row>
          {/* {isOnEdit && (
            <Row>
              <Col>Profile Image</Col>
              <Col>
                <div>
                  <input
                    id="add-image-file-input"
                    type="file"
                    accept="image/*"
                    onChange={this.onChange}
                  />
                </div>
              </Col>
            </Row>
          )} */}
          <Button
            className="update-profile"
            onClick={
              isOnEdit
                ? () => this.props.onCompleteEditing(this.state)
                : this.props.toggleEdit
            }
          >
            {isOnEdit ? "Update" : "Edit"} Profile
          </Button>
        </Col>
      </Row>
    );
  }
}

export default EditProfile;
