import {
  GET_BLOG_INIT,
  GET_BLOG_SUCCESS,
  GET_BLOG_FAILURE,
  SUBSCRIBE_BLOG_INIT,
  SUBSCRIBE_BLOG_SUCCESS,
  SUBSCRIBE_BLOG_FAILURE
} from "./actionTypes";

function getBlogPostInit() {
  return {
    type: GET_BLOG_INIT
  };
}

function getBlogPostSuccess(payload) {
  return {
    type: GET_BLOG_SUCCESS,
    payload
  };
}

function getBlogPostFailure(payload) {
  return {
    type: GET_BLOG_FAILURE,
    payload
  };
}

export function getBlogPost(id) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBlogPostInit());

    let blogService = serviceManager.get("BlogService");

    blogService
      .getBlogPost(id)
      .then(data => {
        dispatch(
          getBlogPostSuccess({
            id,
            data
          })
        );
      }
      )
      .catch(err => {
        dispatch(
          getBlogPostFailure(err)
        );
      });
  };
}

function subscribeToBlogInit() {
  return {
    type: SUBSCRIBE_BLOG_INIT
  };
}

function subscribeToBlogSuccess() {
  return {
    type: SUBSCRIBE_BLOG_SUCCESS
  };
}

function subscribeToBlogFailure(payload) {
  return {
    type: SUBSCRIBE_BLOG_FAILURE,
    payload
  };
}

export function subscribeToBlog(email) {
  return (dispatch, getState, serviceManager) => {
    dispatch(subscribeToBlogInit());

    let blogService = serviceManager.get("BlogService");

    blogService.subscribeToBlog(email)
      .then(() =>
        dispatch(subscribeToBlogSuccess())
      )
      .catch(err =>
        dispatch(subscribeToBlogFailure(err))
      );
  };
}