// @flow
import { createSelector } from "reselect";
import { type ApplicationState } from "../../../store/reducers";
import { type BlogDetailsProps } from "../pages/details";
import { type BlogType } from "../types";

export const getBlogPost = (state: ApplicationState) => state.blog.blog;

export const getBlogPostData = (state: ApplicationState) =>
  state.blog.blog.data;

export const getBlogId = (
  state: ApplicationState,
  props: BlogDetailsProps
) => props.match.params.blogId;

export const getConsolidatedBlog = (createSelector(
  [getBlogPostData],
  (blog) => {
    if (blog !== null) {
      return blog;
    }
  }
): (state: ApplicationState) => BlogType | typeof undefined);