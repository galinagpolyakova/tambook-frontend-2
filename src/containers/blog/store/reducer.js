// @flow
import { type Action } from "../../../shared/types/ReducerAction";

import {
  GET_BLOG_INIT,
  GET_BLOG_SUCCESS,
  GET_BLOG_FAILURE,
  SUBSCRIBE_BLOG_INIT,
  SUBSCRIBE_BLOG_SUCCESS,
  SUBSCRIBE_BLOG_FAILURE
} from "./actionTypes";

export type BlogState = {
  notifications: null | string[],
  blog: Object,
  loading: boolean,
  isLoaded: boolean,
  isBlogLoadingFailure: boolean,
  isSubscriptionSuccess: boolean
}

const initialState: BlogState = {
  notifications: null,
  blog: {
    id: null,
    data: null
  },
  loading: false,
  isLoaded: false,
  isBlogLoadingFailure: false,
  isSubscriptionSuccess: false,
  isSubscriptionLoading: false
};

const reducer = (
  state: BlogState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_BLOG_INIT:
      return getBlogPostInit(state);
    case GET_BLOG_SUCCESS:
      return getBlogPostSuccess(state, payload);
    case GET_BLOG_FAILURE:
      return getBlogPostFailure(state, payload);
    case SUBSCRIBE_BLOG_INIT:
      return subscribeToBlogInit(state);
    case SUBSCRIBE_BLOG_SUCCESS:
      return subscribeToBlogSuccess(state);
    case SUBSCRIBE_BLOG_FAILURE:
      return subscribeToBlogFailure(state);
    default:
      return state;
  }
};

function getBlogPostInit(state: BlogState) {
  return {
    ...state,
    loading: true
  };
}

function getBlogPostSuccess(state: BlogState, { id, data }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    blog: {
      id,
      data
    }
  };
}

function getBlogPostFailure(state: BlogState, { notifications }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    isBlogLoadingFailure: true,
    notifications
  };
}

function subscribeToBlogInit(state: BlogState) {
  return {
    ...state,
    isSubscriptionLoading: true
  };
}

function subscribeToBlogSuccess(state: BlogState) {
  return {
    ...state,
    isSubscriptionSuccess: true,
    isSubscriptionLoading: false
  };
}

function subscribeToBlogFailure(state: BlogState) {
  return {
    ...state,
    isSubscriptionLoading: false
  };
}

export default reducer;