//import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/blog/:blogId",
    className: "blog-details-page",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/details"),
      loading: LoadingComponent
    })
  }
];