// @flow
export type BlogType = {
  activeFrom: string,
  createdDate: number,
  id: number,
  images: Array<string>,
  status: string,
  tags: string,
  blogData: {
    textDetail_en: string,
    title_en: string
  }
};

export type ApiBlogType = {
  activeFrom: string,
  createdDate: number,
  id: number,
  images: Array<{
    path: string,
    lang: string,
    type: string,
    id: number
  }>,
  status: string,
  tags: string,
  textDetail_en: string,
  title_en: string,
  textDetail_ru: string,
  title_ru: string,
  textDetail_es: string,
  title_es: string,
  textDetail_pt: string,
  title_pt: string
};