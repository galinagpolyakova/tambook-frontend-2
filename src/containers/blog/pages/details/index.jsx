// @flow
import { type BlogType } from "../../types";

import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Parser from "html-react-parser";
import { Link } from "react-router-dom";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import PageHeader from "../../../../components/PageHeader";
import Loader from "../../../../components/Loader";

import { isEmail } from "../../../../shared/kernel/cast";
import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Input from "../../../../shared/components/Input";
import Button from "../../../../shared/components/Button";
import Alert from "../../../../shared/components/Alert";
import ImageSlider from "../../../../shared/components/ImageSlider";
import Section, { SectionBody } from "../../../../shared/components/Section";

import { getBlogPost, subscribeToBlog } from "../../store/actions";

import { getConsolidatedBlog } from "../../store/selectors";

import "./styles.css";
import { SITE_URL } from "../../../../config/app";

export type BlogDetailsProps = {
  match: {
    params: {
      blogId: number
    }
  },
  getBlogPost: Function,
  subscribeToBlog: Function,
  blog: BlogType,
  isLoaded: boolean,
  isBlogLoadingFailure: boolean,
  isSubscriptionSuccess: boolean,
  isSubscriptionLoading: boolean
};

type BlogDetailsState = {
  subscribe: {
    email: string,
    emailError: null | string
  }
};
class BlogDetails extends PureComponent<BlogDetailsProps, BlogDetailsState> {
  state = {
    subscribe: {
      email: "",
      emailError: null
    }
  };
  componentDidMount() {
    const {
      isLoaded,
      getBlogPost,
      match: {
        params: { blogId }
      }
    } = this.props;
    !isLoaded ? getBlogPost(blogId) : "";
  }

  componentDidUpdate(prevProps) {
    const { isSubscriptionSuccess } = this.props;
    if (
      prevProps.isSubscriptionSuccess !== isSubscriptionSuccess &&
      isSubscriptionSuccess
    ) {
      this.setState(({ subscribe }) => ({
        subscribe: {
          ...subscribe,
          email: ""
        }
      }));
    }
  }

  handleSubscribeInput = event => {
    this.setState({
      subscribe: {
        ...this.state.subscribe,
        email: event.target.value
      }
    });
  };

  validateSubscribeEmail = () => {
    const { email } = this.state.subscribe;

    let hasError = false;

    this.resetError();

    if (email === "") {
      this.setError("Email is required.");
      hasError = true;
    } else if (!isEmail(email)) {
      this.setError("Email is invalid.");
      hasError = true;
    }
    return hasError;
  };

  setError = (message: string) => {
    this.setState(({ subscribe }) => ({
      subscribe: {
        ...subscribe,
        emailError: message
      }
    }));
  };

  resetError = () => {
    this.setState(({ subscribe }) => ({
      subscribe: {
        ...subscribe,
        emailError: null
      }
    }));
  };

  handleSubscribeSubmit = () => {
    const { subscribeToBlog } = this.props;
    const {
      subscribe: { email }
    } = this.state;

    if (!this.validateSubscribeEmail()) {
      subscribeToBlog({
        email: email
      });
    }
  };

  render() {
    const {
      blog,
      isLoaded,
      isSubscriptionSuccess,
      isSubscriptionLoading,
      isBlogLoadingFailure
    } = this.props;

    const {
      subscribe: { email, emailError }
    } = this.state;

    if (!isLoaded) {
      return <Loader isLoading />;
    }

    if (isBlogLoadingFailure) {
      return (
        <Redirect
          to={{
            pathname: `/404`
          }}
        />
      );
    }

    return (
      <div>
        <PageHeader
          breadcrumbs={[
            {
              title: "BLOG"
            },
            {
              title: "DETAIL"
            }
          ]}
        />
        <div className="blog-article-container">
          <Row>
            <Col size="8">
              <Row>
                <Col>
                  <div className="back-to-blog">
                    <Link to="/under-construction">{"< Back to Blog"}</Link>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-publish-date">{blog.activeFrom}</div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-title">{blog.blogData.title_en}</div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Section className="course-images">
                    <SectionBody>
                      <ImageSlider autoplay slides={blog.images} />
                    </SectionBody>
                  </Section>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-text">
                    {Parser(blog.blogData.textDetail_en)}
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="back-to-blog">
                    <Link to="/under-construction">{"< Back to Blog"}</Link>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col size="4">
              <div className="side-container">
                <Row>
                  <Col>
                    <div className="social-links-heading">SHARE</div>
                    <div className="social-links-container">
                      <ul className="social-links">
                        <li className="social-link-item facebook">
                          <FacebookShareButton url={`${SITE_URL}/blog/1782`}>
                            <i className="fa-fw fab fa-facebook-f" />
                          </FacebookShareButton>
                        </li>
                        <li className="social-link-item linkedin">
                          <LinkedinShareButton>
                            <i className="fa-fw fab fa-linkedin" />
                          </LinkedinShareButton>
                        </li>
                        <li className="social-link-item twitter">
                          <TwitterShareButton>
                            <i className="fa-fw fab fa-twitter" />
                          </TwitterShareButton>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="subscribe-title">
                      {"DON'T MISS A THING:"}
                    </div>
                    <div className="subscribe-description">
                      Subscribe to get our latest Best Offers, Promotions and
                      News
                    </div>
                    <div className="subscribe-input-container">
                      <Input
                        id="email"
                        placeholder="e.g user@example.com"
                        text={email}
                        onChange={event => this.handleSubscribeInput(event)}
                        error={emailError}
                      />
                    </div>
                    <div className="subscribe-button">
                      <Button
                        size={Button.size.MEDIUM}
                        type={Button.type.PRIMARY}
                        onClick={this.handleSubscribeSubmit.bind(this)}
                        loading={isSubscriptionLoading}
                      >
                        SUBSCRIBE
                      </Button>
                    </div>
                    {isSubscriptionSuccess && (
                      <Alert isFullWidth={true} type={Alert.type.SUCCSESS}>
                        Subscribed successfully. Thank you.
                      </Alert>
                    )}
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoaded: state.blog.isLoaded,
    isSubscriptionSuccess: state.blog.isSubscriptionSuccess,
    isSubscriptionLoading: state.blog.isSubscriptionLoading,
    isBlogLoadingFailure: state.blog.isBlogLoadingFailure,
    blog: getConsolidatedBlog(state)
  };
};

const Actions = {
  getBlogPost,
  subscribeToBlog
};

export default connect(
  mapStateToProps,
  Actions
)(BlogDetails);
