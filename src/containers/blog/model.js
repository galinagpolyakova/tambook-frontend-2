// @flow
import type { BlogType, ApiBlogType } from "./types";
import type { SerializableInterface } from "../../shared/SerializableInterface";

import { SITE_URL } from "../../config/app";

export class Blog implements SerializableInterface<BlogType> {
  id: $PropertyType<BlogType, "id">;
  activeFrom: $PropertyType<BlogType, "activeFrom">;
  createdDate: $PropertyType<BlogType, "createdDate">;
  images: $PropertyType<BlogType, "images">;
  status: $PropertyType<BlogType, "status">;
  tags: $PropertyType<BlogType, "tags">;
  blogData: $PropertyType<BlogType, "blogData">;

  constructor({
    id,
    activeFrom,
    createdDate,
    images,
    status,
    tags,
    textDetail_en,
    title_en
  }: ApiBlogType) {
    this.id = id;
    this.activeFrom = activeFrom;
    this.createdDate = createdDate;
    this.images = images.map(image => `${SITE_URL}${image.path}`);
    this.status = status;
    this.tags = tags;
    this.blogData = {
      textDetail_en,
      title_en
    };
  }

  static fromApi(apiBlog: ApiBlogType) {
    return new this(apiBlog);
  }

  toJSON(): BlogType {
    return this;
  }
}