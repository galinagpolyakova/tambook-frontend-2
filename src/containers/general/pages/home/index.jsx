// @flow
import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";

import {
  getFeaturedCourses,
  getFilterParams
} from "../../../course/store/actions";
import { getFeaturedReviews } from "../../../review/store/actions";

import { getCourseParams } from "../../../course/store/selectors";

import type { ConsolidatedCoursesListType } from "../../../course/types";
import type { ReviewsListType } from "../../../review/types";

import "./styles.css";

import MainSlider from "./partials/MainSlider";
//import ExploreSection from "./partials/ExploreSection";
//import CountriesSection from "./partials/CountriesSection";
//import FeaturedClasses from "./partials/FeaturedClasses";
import LanguageCourses from "./partials/LanguageCourses";
import WhatPeopleAreSaying from "./partials/WhatPeopleAreSaying";

type HomeProps = {
  getFeaturedCourses: Function,
  getFeaturedReviews: Function,
  featuredCourses: ConsolidatedCoursesListType,
  featuredReviews: ReviewsListType,
  filters: Object | null,
  getFilterParams: Function
};

class Home extends PureComponent<HomeProps> {
  componentDidMount() {
    this.props.getFeaturedCourses();
    this.props.getFeaturedReviews();
    if (this.props.filters === null) {
      this.props.getFilterParams();
    }
  }

  render() {
    const { featuredReviews } = this.props;
    return (
      <Fragment>
        <MainSlider
          countries={
            this.props.filters === null ? [] : this.props.filters.countries
          }
          languages={
            this.props.filters === null ? [] : this.props.filters.languages
          }
        />
        <div>
          {/*<ExploreSection />*/}
          {/*<CountriesSection />*/}
          {/*<FeaturedClasses featuredCourses={featuredCourses} />*/}
          <LanguageCourses />
          <WhatPeopleAreSaying featuredReviews={featuredReviews} />
        </div>
      </Fragment>
    );
  }
}
const Actions = {
  getFeaturedCourses,
  getFeaturedReviews,
  getFilterParams
};
function mapStateToProps(state) {
  return {
    featuredCourses: state.course.featuredCourses,
    featuredReviews: state.review.featuredReviews,
    filters: getCourseParams(state)
  };
}
export default connect(
  mapStateToProps,
  Actions
)(Home);
