// @flow
import type { ReviewType } from "../../../../../review/types";

import React, { PureComponent } from "react";

import Rate from "../../../../../../shared/components/Rate";
import Card, {
  CardImage,
  CardBody
} from "../../../../../../shared/components/Card";

import { textTruncate } from "../../../../../../shared/utils";

import "./styles.css";

type ReviewCardProps = ReviewType;
class ReviewCard extends PureComponent<ReviewCardProps> {
  render() {
    const { tag, profilePic, rating, comment, name, image } = this.props;
    return (
      <Card className="review-card">
        <CardImage image={image}>
          <div className="course-title">{tag}</div>
        </CardImage>
        <CardBody>
          <div className="reviewer-image">
            <img alt={name} src={profilePic} />
          </div>
          <div className="reviewer-rating">
            <Rate value={rating} size="small" />
          </div>
          <div className="reviewer-name">{name}</div>
          <div className="review-comment">{textTruncate(comment, 200)}</div>
          <br />
        </CardBody>
      </Card>
    );
  }
}

export default ReviewCard;
