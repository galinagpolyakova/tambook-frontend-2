// @flow
import React, { PureComponent } from "react";
import Section from "../../../../../shared/components/Section/Section";
import SectionBody from "../../../../../shared/components/Section/SectionBody";
import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import CourseCard from "../../../../../components/CourseCard";

import type { ConsolidatedCoursesListType } from "./../../../../course/types";

type FeaturedClassesProps = {
  featuredCourses: ConsolidatedCoursesListType
};

class FeaturedClasses extends PureComponent<FeaturedClassesProps> {
  getCourse(featuredCourses: ConsolidatedCoursesListType) {
    if (featuredCourses !== undefined && featuredCourses.length > 0) {
      return featuredCourses.map((featuredCourse, key) => (
        <Col key={key}>
          <CourseCard {...featuredCourse} />
        </Col>
      ));
    }
  }
  render() {
    const { featuredCourses } = this.props;
    return (
      <Section>
        <div className="bg-primary">
          <SectionBody className="feature-classes-section container">
            <Row>
              <Col>
                <p className="heading-1">FEATURED CLASSES</p>
                <p className="heading-6">
                  Browse some of our featured classes as chosen by outstanding
                  student feedback and popular locations.
                </p>
              </Col>
              {this.getCourse(featuredCourses)}
            </Row>
          </SectionBody>
        </div>
      </Section>
    );
  }
}

export default FeaturedClasses;
