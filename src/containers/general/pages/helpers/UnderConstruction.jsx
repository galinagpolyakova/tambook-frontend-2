import React, { Component } from "react";

import Section from "../../../../shared/components/Section";

import underConstruction from "../../../../assets/under-construction.svg";

import "./styles.css";

class UnderConstruction extends Component {
  render() {
    return (
      <div className="container">
        <Section className="helper-page">
          <img src={underConstruction} />
          <div className="heading-4">This page is currently</div>
          <div className="heading-2">under construction</div>
        </Section>
      </div>
    );
  }
}

export default UnderConstruction;
