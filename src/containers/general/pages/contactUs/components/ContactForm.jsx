// @flow
import React, { PureComponent } from 'react';

import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Input from "../../../../../shared/components/Input";
import Select from "../../../../../shared/components/Select";
import Checkbox from "../../../../../shared/components/Checkbox";
import Button from "../../../../../shared/components/Button";
import Textarea from "../../../../../shared/components/Textarea";
import Alert from "../../../../../shared/components/Alert";
import { isEmail } from "../../../../../shared/kernel/cast";

import Section, {
  SectionHeader
} from "../../../../../shared/components/Section";

type ContactFormProps = {
  sendContactMessage: Function,
  isContactMessageSendSuccess: boolean,
  isLoading: boolean
}

type ContactFormState = {
  form: {
    name: string,
    email: string,
    personType: string,
    message: string,
    promotions: boolean
  },
  formErrors: {
    name: null | string,
    email: null | string,
    message: null | string
  }
}
class ContactForm extends PureComponent<ContactFormProps, ContactFormState> {

  state = {
    form: {
      name: "",
      email: "",
      personType: "Student",
      message: "",
      promotions: false
    },
    formErrors: {
      name: null,
      email: null,
      message: null
    }
  }

  componentDidUpdate(prevProps: any) {
    const { isContactMessageSendSuccess } = this.props;
    if (prevProps.isContactMessageSendSuccess !== isContactMessageSendSuccess
      && isContactMessageSendSuccess
    ) {
      this.resetForm();
    }
  }

  handleFormFieldChange = (event: any) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.id]: event.target.value
      }
    });
  };

  handleFormSelectChange = (personType: string) => {
    this.setState(
      ({ form }) => ({
        form: {
          ...form,
          personType: personType
        }
      })
    );
  };

  handleFormCheckboxChange = () => {
    const prevPromotion = this.state.form.promotions;
    this.setState(
      ({ form }) => ({
        form: {
          ...form,
          promotions: !prevPromotion
        }
      })
    );
  }

  handleFormSubmit = () => {
    const { sendContactMessage } = this.props;
    if (!this.validateForm()) {
      const {
        form: {
          name,
          email,
          personType,
          message,
          promotions
        }
      } = this.state;
      sendContactMessage({
        name,
        email,
        personType,
        message,
        promotions
      });
    }
  }

  resetForm = () => {
    this.setState(
      ({ form }) => ({
        form: {
          ...form,
          name: "",
          email: "",
          personType: "Student",
          message: "",
          promotions: false
        }
      })
    );
  }

  resetFormErrors = () => {
    this.setState(
      ({ formErrors }) => ({
        formErrors: {
          ...formErrors,
          name: null,
          email: null,
          message: null
        }
      })
    );
  }

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  validateForm = () => {
    const {
      name,
      email,
      message
    } = this.state.form;

    let hasError = false;

    this.resetFormErrors();

    if (name === "") {
      this.setFormErrors("name", "Name is required.");
      hasError = true;
    }

    if (email === "") {
      this.setFormErrors("email", "Email is required.");
      hasError = true;
    } else if (!isEmail(email)) {
      this.setFormErrors("email", "Email is invalid.");
      hasError = true;
    }

    if (message === "") {
      this.setFormErrors("message", "Message is required.");
      hasError = true;
    }
    return hasError;
  };

  render() {
    const {
      isLoading,
      isContactMessageSendSuccess
    } = this.props;
    const {
      form,
      formErrors
    } = this.state;
    return (
      <Section>
        <SectionHeader
          heading="Get in touch"
          position="left"
        />
        <p>Fill out the form below and we will be in touch straight away.</p>
        <Row>
          <Col>
            <div className="input-group">
              <label>Your Name</label>
              <Input
                id="name"
                text={form.name}
                onChange={this.handleFormFieldChange.bind(this)}
                error={formErrors.name}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="input-group">
              <label>Your Email</label>
              <Input
                id="email"
                text={form.email}
                onChange={this.handleFormFieldChange.bind(this)}
                error={formErrors.email}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col size="4">
            <div className="input-group">
              <label>Contact Tambook As</label>
              <Select
                options={["Student", "School", "Agent"]}
                selected={form.personType}
                onChange={personType => {
                  this.handleFormSelectChange(personType);
                }}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="input-area-group">
              <label>Your Message</label>
              <Textarea
                id="message"
                type="textarea"
                text={form.message}
                onChange={this.handleFormFieldChange.bind(this)}
                error={formErrors.message}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Checkbox
              text="I want to receive Tambook promotional offers in the future"
              isChecked={form.promotions}
              onChange={this.handleFormCheckboxChange.bind(this)}
            />
          </Col>
        </Row>
        <Row className="form-submit">
          <Col>
            <Button
              size={Button.size.MEDIUM}
              type={Button.type.PRIMARY}
              loading={isLoading}
              onClick={this.handleFormSubmit.bind(this)}
            >
              SEND MESSAGE
            </Button>
          </Col>
        </Row>
        {isContactMessageSendSuccess && (
          <Alert isFullWidth={true} type={Alert.type.SUCCSESS}>
            Your message has been successfully sent. Thank you.
          </Alert>
        )}
      </Section>
    );
  }

}

export default ContactForm;
