import React, { PureComponent } from 'react';

import Section, { SectionBody } from "../../../../../shared/components/Section";
import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Button from "../../../../../shared/components/Button";

import AboutUsSideBar from "../components/AboutUsSidebar";

class DetailsSection extends PureComponent {
  render() {
    return (
      <div className="detail-content">
        <Row>
          <Col size="8">
            <Section className="section-advantages">
              <SectionBody>
                <Row className="advantage-item">
                  <Col size="2">
                    <i className="fas fa-user-ninja" />
                  </Col>
                  <Col size="10" className="item-description">
                    Want to learn a new skill on the adventure of a lifetime?
                  </Col>
                </Row>
                <Row className="advantage-item">
                  <Col size="2">
                    <i className="fas fa-dollar-sign" />
                  </Col>
                  <Col size="10" className="item-description">
                    Tambook makes booking language courses around the world easy and affordable.
                  </Col>
                </Row>
                <Row className="advantage-item">
                  <Col size="2">
                    <i className="fas fa-graduation-cap" />
                  </Col>
                  <Col size="10" className="item-description">
                    Hundreds of schools, thousands of courses, and amazing best price offers.
                  </Col>
                </Row>
                <Row className="advantage-item">
                  <Col size="2">
                    <i className="far fa-compass" />
                  </Col>
                  <Col size="10" className="item-description">
                    Everything you need to get your adventure started.
                  </Col>
                </Row>
              </SectionBody>
            </Section>
            <Section>
              <SectionBody>
                <p>At Tambook, we believe in globally-conscious learners.
                We help ambitious people set their own rules for life, and
                become the most outstanding versions of themselves in the process.</p>

                <p>As a team of expats from Eurasia and South America, we
                learned firsthand how frustrating, time-consuming, and expensive
                it can be to find a language course in a different country. After
                scrolling through hundreds of websites, we paid hefty fees to haggle
                with money-grabbing agents. Talking directly with schools was
                difficult - especially as we didn’t speak the language yet.</p>

                <p>In 2015, we built Tambook to flip this status quo. Trusted by
                students and language schools around the globe, we designed Tambook
                to be a state-of-the-art booking platform that anyone could use to
                compare language courses, score the best prices directly from schools,
                and pay in their local currency within a single browsing session.</p>

                <p>Driven by a pioneering spirit, we’re always working hard to make
                things better for eager learners. Our team will keep innovating until
                studying a language abroad becomes an easy, affordable option for everyone.</p>

                <p>Tambook HQ is currently located in New Zealand. We have a lot of
                team members working here, and a lot of team members working all around
                the world. We’re here to make booking a language course easy for you, so
                if you have any questions, please reach out.</p>
              </SectionBody>
            </Section>
            <Section>
              <SectionBody>
                <Button
                  type={Button.type.SECONDARY}
                  size={Button.size.SMALL}
                  htmlType={Button.htmlType.LINK}
                  link={`/filter?country=&language=`}
                >START YOUR ADVENTURE NOW</Button>
              </SectionBody>
            </Section>
          </Col>
          <Col size="4">
            <Section>
              <AboutUsSideBar />
            </Section>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailsSection;
