// @flow
import React from 'react';

import Sidebar, { Widget } from "../../../../../../components/Sidebar";
import Map from "../../../../../../shared/components/Map.jsx";

import "./styles.css";

const AboutUsSideBar = () => {
  const LATITUDE = -36.861844;
  const LONGITUDE = -185.237911;
  return (
    <Sidebar>
      <Widget className="detail-container" title="NEED HELP?" type={Widget.types.LIGHT}>
        <p>
          Look up <a href="#">frequently asked questions</a> for a hint.
        </p>
        <div className="number-line" />
        <p>
          Our friendly and experienced student advisors will help you with all your questions
        </p>
        <div className="contact-details">
          <div className="contact-option">
            <i className="far fa-envelope" />
            OFFICE@TAMBOOK.COM
          </div>
        </div>
        <div className="number-line" />
        <h4>New Zealand Head Office</h4>
        <div className="center-details">
          <div className="center-option">
            <i className="fas fa-map-marker-alt" />
            3 Glenside Crescent, Eden Terrace, Auckland, NZ.
          </div>
          <div className="center-option">
            <i className="fas fa-envelope" />
            office@tambook.com
          </div>
        </div>
      </Widget>
      <Widget>
        <div style={{ minHeight: "200px", width: "100%" }}>
          <Map
            lat={LATITUDE}
            lng={LONGITUDE}
          />
        </div>
      </Widget>
    </Sidebar>
  );
};

export default AboutUsSideBar;