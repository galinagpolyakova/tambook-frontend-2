// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Loader from "../../../../components/Loader";
import PageHeader from "../../../../components/PageHeader";
import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Input from "../../../../shared/components/Input";
import Accordion from "../../../../shared/components/Accordion";
import Button from "../../../../shared/components/Button";

import { getAllFaq } from "../../store/actions";
import {
  getFeaturedCourses,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList
} from "../../../course/store/actions";
import FeaturedCourses from "../termsAndConditions/partials/featuredCourses";
import { type ConsolidatedFaqType } from "../../types";
import { type ConsolidatedCoursesListType } from "../../../course/types";

import "./styles.css";

type FaqPropsType = {
  getAllFaq: Function,
  isGetFaqLoading: boolean,
  isFaqLoaded: boolean,
  faq: Array<ConsolidatedFaqType>,
  getFeaturedCourses: Function,
  featuredCourses: ConsolidatedCoursesListType,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  isLoaded: boolean
}

type FaqStateType = {
  form: {
    input: string
  },
  result: {
    group: string,
    questions: Array<any>
  }
}
class Faq extends PureComponent<FaqPropsType, FaqStateType> {
  EMPTY_SEARCH = "SEARCH: NO RESULT FOUND"

  state = {
    form: {
      input: ""
    },
    result: {
      group: "",
      questions: []
    }
  }

  componentDidMount() {
    const { getAllFaq, isFaqLoaded, isLoaded, getFeaturedCourses } = this.props;

    !isLoaded ? getFeaturedCourses() : "";
    !isFaqLoaded ? getAllFaq() : "";

    this.setInitialFaq();

  }

  componentDidUpdate(prevProps) {
    const { isFaqLoaded } = this.props;

    if (prevProps.isFaqLoaded !== isFaqLoaded
      && isFaqLoaded
    ) {
      this.setInitialFaq();
    }
  }

  setInitialFaq = () => {
    const { faq } = this.props;

    if (faq.length) {
      this.setState(
        ({ result }) => ({
          result: {
            ...result,
            group: faq[0].group,
            questions: faq[0].questions
          }
        })
      );
    }
  }

  searchQuestion = () => {
    const { form: { input } } = this.state;
    const { faq } = this.props;
    let matched = false;

    faq.map(question => {
      if (question.group.toUpperCase() == input.toUpperCase()) {
        let group = "SEARCH: " + question.group;
        matched = true;
        this.onSearchListItem(group, question.questions);
      }
    });
    { !matched ? this.onSearchListItem(this.EMPTY_SEARCH, []) : null; }
  }

  onSearchListItem = (group, questions) => {
    this.setState(
      ({ result }) => ({
        result: {
          ...result,
          group: group,
          questions: questions
        }
      })
    );
  }

  onClickListItem = ({ group, questions }) => {
    this.setState(
      ({ result }) => ({
        result: {
          ...result,
          group: group,
          questions: questions
        }
      })
    );
  }

  getQuestionsList = () => {
    const items = [];
    const { result: { questions } } = this.state;

    questions.map(question => {
      let item = {
        title: question.question_en,
        content: (<div>{question.answer_en}</div>)
      };
      items.push(item);
    });
    return items;
  }

  handleInput = (event) => {
    this.setState({
      form: {
        ...this.state.form,
        input: event.target.value
      }
    });
  }

  render() {
    const { isFaqLoaded, faq, featuredCourses, isLoaded } = this.props;
    const { result, form: { input } } = this.state;
    const items = this.getQuestionsList();

    if (!(isFaqLoaded && isLoaded)) {
      return <Loader isLoading />;
    }

    return (
      <div>
        <PageHeader
          title="FREQUENTLY ASKED QUESTIONS"
          breadcrumbs={[
            {
              title: "FAQ"
            }
          ]}
        />
        <div className="faq-container">
          <Row>
            <Col size="4">
              <div className="category-container">
                <div className="search-container">
                  <Row>
                    <Col size="9">
                      <Input
                        placeholder="Search"
                        text={input} x
                        onChange={this.handleInput.bind(this)}
                      />
                    </Col>
                    <Col size="3">
                      <Button
                        onClick={this.searchQuestion.bind(this)}
                      >
                        <i className="fas fa-search" />
                      </Button>
                    </Col>
                  </Row>
                </div>
                <h2>CATEGORIES</h2>
                <ul type="none">
                  {faq.map(({ group, questions }, key) => (
                    <li
                      key={key}
                      onClick={() => this.onClickListItem({ group, questions })}
                    >
                      {group}
                    </li>
                  ))}
                </ul>
              </div>
            </Col>
            <Col size="8">
              <h1>{result.group}</h1>
              <Accordion items={items} />
            </Col>
          </Row>
          <Row>
            <Col>
              {isLoaded ? (
                <FeaturedCourses
                  featuredCourses={featuredCourses}
                  addCourseToBookmarkList={addCourseToBookmarkList}
                  removeCourseFromBookmarkList={removeCourseFromBookmarkList}
                />) : ""}
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const Actions = {
  getFeaturedCourses,
  getAllFaq,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList
};

const mapStateToProps = (state) => {
  return {
    isGetFaqLoading: state.general.isGetFaqLoading,
    isFaqLoaded: state.general.isFaqLoaded,
    faq: state.general.faq,
    featuredCourses: state.course.featuredCourses,
    isLoaded: state.course.isLoaded
  };
};

export default connect(
  mapStateToProps,
  Actions
)(Faq);
