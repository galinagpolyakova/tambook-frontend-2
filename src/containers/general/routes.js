// import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/",
    exact: true,
    className: "home-page",
    component: Loadable({
      loader: () => import("./pages/home"),
      loading: LoadingComponent
    })
  },
  {
    path: "/about-us",
    exact: true,
    className: "about-us-page",
    component: Loadable({
      loader: () => import("./pages/aboutUs"),
      loading: LoadingComponent
    })
  },
  {
    path: "/faq",
    exact: true,
    className: "faq-page",
    component: Loadable({
      loader: () => import("./pages/faq"),
      loading: LoadingComponent
    })
  },
  {
    path: "/under-construction",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/helpers/UnderConstruction"),
      loading: LoadingComponent
    })
  },
  {
    path: "/contact-us",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/contactUs"),
      loading: LoadingComponent
    })
  },
  {
    path: "/terms-and-conditions",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/termsAndConditions"),
      loading: LoadingComponent
    })
  },
  {
    path: "/404",
    exact: true,
    component: Loadable({
      loader: () => import("./pages/helpers/404"),
      loading: LoadingComponent
    })
  }
];
