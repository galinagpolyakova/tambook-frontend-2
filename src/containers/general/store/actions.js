import {
  SEND_CONTACT_MESSAGE_INIT,
  SEND_CONTACT_MESSAGE_SUCCESS,
  SEND_CONTACT_MESSAGE_FAILURE,
  GET_ALL_FAQ_INIT,
  GET_ALL_FAQ_SUCCESS,
  GET_ALL_FAQ_FAILURE
} from "./actionTypes";

export function sendContactMessage(messageDetails) {
  return (dispatch, getState, serviceManager) => {
    dispatch(sendContactMessageInit());

    let generalService = serviceManager.get("GeneralService");
    generalService
      .sendContactMessage(messageDetails)
      .then(() => {
        dispatch(
          sendContactMessageSuccess()
        );
      }
      )
      .catch(err => {
        dispatch(sendContactMessageFailure(err));
      });
  };
}

function sendContactMessageInit() {
  return {
    type: SEND_CONTACT_MESSAGE_INIT
  };
}

function sendContactMessageSuccess() {
  return {
    type: SEND_CONTACT_MESSAGE_SUCCESS,
  };
}

function sendContactMessageFailure() {
  return {
    type: SEND_CONTACT_MESSAGE_FAILURE
  };
}

export function getAllFaq() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getAllFaqInit());

    let generalService = serviceManager.get("GeneralService");
    generalService
      .getAllFaq()
      .then(data => {
        dispatch(getAllFaqSuccess(data));
      })
      .catch(err =>
        dispatch(getAllFaqFailure(err))
      );
  };
}

function getAllFaqInit() {
  return {
    type: GET_ALL_FAQ_INIT
  };
}

function getAllFaqSuccess(payload) {
  return {
    type: GET_ALL_FAQ_SUCCESS,
    payload
  };
}

function getAllFaqFailure(payload) {
  return {
    type: GET_ALL_FAQ_FAILURE,
    payload
  };
}