// @flow
export type ApiFaqType = {
  group: string,
  categoury_pt: string,
  categoury_ru: string,
  categoury_sp: string,
  date: number,
  list: Array<{
    answer_sp: string,
    categoury_pt: string,
    categoury_ru: string,
    createdAt: number,
    isDeleted: string,
    question_en: string,
    categoury_sp: string,
    question_ru: string,
    categoury_en: string,
    answer_en: string,
    question_pt: string,
    answer_ru: string,
    addCategoury: string,
    id: number,
    question_sp: string,
    answer_pt: string
  }>
}

export type ApiFaqListType = Array<ApiFaqType>;

export type ConsolidatedFaqType = {
  group: string,
  questions: Array<{
    question_en: string,
    answer_en: string
  }>
}

export type ConsolidatedFaqListType = Array<ConsolidatedFaqType>;