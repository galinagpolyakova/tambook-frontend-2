// @flow
import React from 'react';

type ProgressBarProps = {
  step: number
}

const ProgressBar = ({ step }: ProgressBarProps) => {
  return (
    <div className="progress-bar-container">
      <ul className="progress-bar-block">
        <li className={`progress-bar-elements ${step > 0 ? 'active' : ''}`}>Course</li>
        <li className={`progress-bar-elements ${step > 1 ? 'active' : ''}`}>Accomodation</li>
        <li className={`progress-bar-elements ${step > 2 ? 'active' : ''}`}>Travel</li>
        <li className={`progress-bar-elements ${step > 3 ? 'active' : ''}`}>Personal Info</li>
        <li className={`progress-bar-elements ${step > 4 ? 'active' : ''}`}>Payment</li>
      </ul>
    </div>
  );
};

export default ProgressBar;
