// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { StripeProvider, Elements } from "react-stripe-elements";

import PageHeader from "../../../../components/PageHeader";
import CheckoutForm from "../../components/CheckoutForm";
import Alert from "../../../../shared/components/Alert";

import { finalizePayment, showPaymentError } from "../../store/actions";

import "../../styles.css";
import { STRIPE_KEY } from "../../../../config/stripe";

type PaymentProps = {
  isPaymentSuccess: boolean,
  isPaymentLoading: boolean,
  finalizePayment: Function,
  showPaymentError: Function,
  match: any,
  error: null | string
};

class Payment extends Component<PaymentProps> {
  render() {
    const {
      match: {
        params: { bookingId }
      },
      isPaymentLoading,
      error,
      isPaymentSuccess
    } = this.props;
    return (
      <Fragment>
        <PageHeader
          title="COURSE BOOKING"
          breadcrumbs={[
            {
              title: "Booking"
            }
          ]}
        />
        <div className="container">
          {isPaymentSuccess ? (
            <Alert isFullWidth={true} type={Alert.type.SUCCSESS}>
              Your payment has been successfully processed. Thank you.
            </Alert>
          ) : (
            <StripeProvider apiKey={STRIPE_KEY}>
              <div className="checkout">
                <Elements>
                  <CheckoutForm
                    isLoading={isPaymentLoading}
                    handleResult={({ token, error }) => {
                      error
                        ? this.props.showPaymentError({
                            error: error.message
                          })
                        : this.props.finalizePayment({
                            cardToken: token.id,
                            bookingId
                          });
                    }}
                  />
                </Elements>
                {error && (
                  <Alert isFullWidth={true} type={Alert.type.ERROR}>
                    {error}
                  </Alert>
                )}
              </div>
            </StripeProvider>
          )}
        </div>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    isPaymentSuccess: state.booking.isPaymentSuccess,
    isPaymentLoading: state.booking.isPaymentLoading,
    error: state.booking.error
  };
}

const Action = {
  finalizePayment,
  showPaymentError
};

export default connect(
  mapStateToProps,
  Action
)(Payment);
