// @flow
import React, { PureComponent } from "react";

import Button from "../../../../shared/components/Button";
import RadioButton from "../../../../shared/components/RadioButton";

import "./styles.css";

type PaymentSchemeFormProps = {
  prevStep: Function,
  handleFormSubmit: Function,
  handlePaymentMethodChange: Function,
  loading: boolean
}

class PaymentSchemeForm extends PureComponent<PaymentSchemeFormProps> {
  STRIPE = "stripe";

  componentDidMount() {
    const { handlePaymentMethodChange } = this.props;
    handlePaymentMethodChange(this.STRIPE);
  }

  render() {
    const {
      prevStep,
      handleFormSubmit,
      loading
    } = this.props;
    return (
      <div>
        <p className="heading-4">PAYMENT SCHEME</p>
        <div className="payment-scheme-container">
          <RadioButton
            label="Full (100%) course prepayment"
            checked={true}
          />
        </div>
        <p className="heading-4">PAYMENT METHOD</p>
        <div className="payment-method-container">
          <RadioButton
            label="Credit Card"
            checked={true}
          />
        </div>
        <div className="button-block">
          <Button
            size={Button.size.MEDIUM}
            type={Button.type.SECONDARY}
            onClick={prevStep}
          >
            STEP BACK
          </Button>
          <Button
            size={Button.size.MEDIUM}
            type={Button.type.PRIMARY}
            onClick={handleFormSubmit}
            loading={loading}
          >
            CONFIRM AND PAY
          </Button>
        </div>
      </div>
    );
  }
}

export default PaymentSchemeForm;