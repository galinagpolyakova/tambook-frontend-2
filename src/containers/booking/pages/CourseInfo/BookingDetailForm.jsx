// @flow
import React, { PureComponent } from "react";

import { type CourseType, type CourseFeeType } from "../../../course/types";

import Select from "../../../../shared/components/Select";
import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Currency from "../../../../components/Currency";
import Icon from "../../../../shared/components/Icon";
import Button from "../../../../shared/components/Button";
import { Rating } from "../../../../shared/components/Rate";
import { Datepicker } from "../../../../shared/components/Datepicker/Datepicker";

import addWeeks from "../../../../shared/utils/dates/addWeeks";
import format from "../../../../shared/utils/dates/format";

import "./styles.css";

type BookingDetailFormProps = {
  course: CourseType,
  courseStartDate: Date | null,
  selectedWeek: number | null,
  isFixedCourse: boolean,
  fees: any,
  selectedRange: number | null,
  range: CourseFeeType | null,
  options: Array<any>,
  onRangeSelect: Function,
  handleCourseStartDate: Function,
  handleSelectedWeek: Function,
  nextStep: Function
};
type BookingDetailFormState = {
  formErrors: {
    courseStartDate: null | string
  }
};
class BookingDetailForm extends PureComponent<
  BookingDetailFormProps,
  BookingDetailFormState
> {
  state = {
    formErrors: {
      courseStartDate: null
    }
  };
  validateForm = () => {
    const { courseStartDate } = this.props;

    let hasError = false;

    this.resetFormErrors();

    if (courseStartDate === null) {
      this.setFormErrors("courseStartDate", "Start date is required.");
      hasError = true;
    }
    return hasError;
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  resetFormErrors = () => {
    this.setState({
      formErrors: {
        courseStartDate: null
      }
    });
  };

  nextStep = () => {
    const { nextStep } = this.props;
    if (!this.validateForm()) {
      nextStep();
    }
  };

  render() {
    const {
      course,
      courseStartDate,
      selectedWeek,
      isFixedCourse,
      fees,
      selectedRange,
      range,
      options,
      onRangeSelect,
      handleCourseStartDate,
      handleSelectedWeek
    } = this.props;
    const { formErrors } = this.state;
    return (
      <div>
        <p className="heading-4">YOUR BOOKING DETAILS</p>
        <p className="heading-6">{course.title}</p>
        <Rating
          rate={course.rating.rate}
          reviews={course.rating.reviews}
          size={Rating.size.SMALL}
        />
        {!isFixedCourse && (
          <div className="booking-course-duration">
            <div className="duration-selector">
              <Row>
                <Col size={4}>
                  <div className="duration-selector-title">WEEK RANGE</div>
                </Col>
                <Col size={4}>
                  <Select
                    options={fees}
                    onChange={key => onRangeSelect(key)}
                    selected={selectedRange}
                  />
                </Col>
              </Row>
            </div>
          </div>
        )}
        {selectedWeek !== null && range !== null && (
          <div className="booking-course-duration">
            <div className="duration-selector">
              <Row>
                <Col size={4}>
                  <div className="duration-selector-title">HOW MANY WEEKS</div>
                </Col>
                <Col size={4}>
                  <Select
                    onChange={selectedWeek => handleSelectedWeek(selectedWeek)}
                    selected={selectedWeek}
                    options={options}
                  />
                </Col>
                <Col size={4}>
                  <div className="duration-selector-content">
                    <span>
                      @{" "}
                      <Currency currencyType={course.currencyType}>
                        {range.price}
                      </Currency>
                      /Week
                    </span>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        )}
        <div className="booking-course-duration">
          <div className="duration-selector">
            <Row>
              <Col size={4}>
                <div className="duration-selector-title">
                  PICK YOUR START DATE
                </div>
              </Col>
              <Col size={4}>
                <div className="date-selector-content">
                  <Datepicker
                    onToggleDate={courseStartDate => {
                      handleCourseStartDate(courseStartDate);
                    }}
                    initialSelection={courseStartDate}
                    error={formErrors.courseStartDate}
                  />
                </div>
              </Col>
              {courseStartDate !== null && selectedWeek !== "0" && (
                <Col size={4}>
                  <div className="duration-selector-content duration-end-date">
                    <div className="icon-prefix">
                      <Icon icon="arrow-right" />
                    </div>
                    <Icon icon="calendar-alt" />
                    <span className="duration-selector-date">
                      {format(
                        addWeeks(new Date(courseStartDate), selectedWeek),
                        "yyyy-MM-dd"
                      )}
                    </span>
                  </div>
                </Col>
              )}
            </Row>
          </div>
        </div>
        <div className="note">
          * If you require a visa to enter New Zealand or Australia, please make
          sure you have allowed enough time to obtain one.
        </div>
        <div className="button-single">
          <Button
            size={Button.size.MEDIUM}
            type={Button.type.SECONDARY}
            onClick={this.nextStep.bind(this)}
          >
            NEXT STEP
          </Button>
        </div>
      </div>
    );
  }
}

export default BookingDetailForm;
