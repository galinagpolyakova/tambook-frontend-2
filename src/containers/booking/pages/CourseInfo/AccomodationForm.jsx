// @flow
import React, { PureComponent } from "react";

import { type CourseType } from "../../../course/types";

import RadioButton from "../../../../shared/components/RadioButton";
import Button from "../../../../shared/components/Button";
import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Select from "../../../../shared/components/Select";

import Currency from "../../../../components/Currency";

import { Datepicker } from "../../../../shared/components/Datepicker/Datepicker";
import format from "../../../../shared/utils/dates/format";
import addWeeks from "../../../../shared/utils/dates/addWeeks";

type AccomodationFormProps = {
  course: CourseType,
  selectedWeek: number | null,
  handleHomeStayDates: Function,
  options: Array<any>,
  handleHomeStayPreferenceChange: Function,
  handleAccommodationPreferences: Function,
  onClickHomestayPreference: Function,
  handleAccomodationSelectedWeek: Function,
  accommodation: Object,
  checked: string,
  showPreferences: boolean,
  selectOptions: Array<string>,
  nextStep: Function,
  prevStep: Function
};
type AccomodationFormState = {
  formErrors: {
    startDate: null | string,
    endDate: null | string
  }
};

class AccomodationForm extends PureComponent<
  AccomodationFormProps,
  AccomodationFormState
  > {
  OPTION1 = "option1";
  OPTION2 = "option2";
  OPTION3 = "option3";

  state = {
    formErrors: {
      startDate: null,
      endDate: null
    }
  };

  validateForm = () => {
    const {
      accommodation: { startDate, endDate }
    } = this.props;

    let hasError = false;

    this.resetFormErrors();

    if (startDate === null) {
      this.setFormErrors("startDate", "Start date is required.");
      hasError = true;
    }
    if (endDate === null) {
      this.setFormErrors("endDate", "Number of weeks is required.");
      hasError = true;
    }
    return hasError;
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  resetFormErrors = () => {
    this.setState({
      formErrors: {
        startDate: null,
        endDate: null
      }
    });
  };

  handleSelectedWeek = (selectedWeek: number) => {
    const {
      accommodation,
      handleHomeStayDates,
      handleAccomodationSelectedWeek
    } = this.props;

    let endDate = format(
      addWeeks(
        new Date(
          accommodation.startDate
        ),
        selectedWeek),
      "yyyy-MM-dd"
    );
    handleHomeStayDates(endDate, "endDate");
    handleAccomodationSelectedWeek(selectedWeek);
  }

  getHomestayPreferences = () => {
    const {
      selectOptions,
      accommodation,
      handleHomeStayDates,
      handleHomeStayPreferenceChange,
      options
    } = this.props;

    const { formErrors } = this.state;
    return (
      <div>
        <div className="homestay-duration">
          <p className="heading-4">HOMESTAY DURATION</p>
          <Row>
            <Col size="6">
              <div className="homestay-date-label">Start Date</div>
              <div className="homestay-date-picker">
                <Datepicker
                  onToggleDate={homeStayStartDate => {
                    handleHomeStayDates(homeStayStartDate, "startDate");
                  }}
                  initialSelection={accommodation.startDate}
                  error={formErrors.startDate}
                />
              </div>
            </Col>
            <Col size="6">
              <div className="homestay-date-label">How Many Weeks</div>
              <div className="homestay-date-picker">
                <Select
                  onChange={selectedWeek => this.handleSelectedWeek(selectedWeek)}
                  selected={accommodation.homeStayWeeks}
                  options={options}
                  error={formErrors.endDate}
                  placeholder="select"
                />
              </div>
            </Col>
          </Row>
        </div>
        <div className="homestay-preferences">
          <p className="heading-4">HOMESTAY PREFERENCES</p>
          <Row>
            <Col size="4">
              <div className="homestay-preferences-label">
                Family with younger children
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.youngChildren}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "youngChildren")
                  }
                />
              </div>
            </Col>
            <Col size="4">
              <div className="homestay-preferences-label">
                Family with older children
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.olderChildren}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "olderChildren")
                  }
                />
              </div>
            </Col>
            <Col size="4">
              <div className="homestay-preferences-label">
                Older adults, Children left home
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.olderAdults}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "olderAdults")
                  }
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col size="4">
              <div className="homestay-preferences-label">
                Do you have pets?
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.pets}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "pets")
                  }
                />
              </div>
            </Col>
            <Col size="4">
              <div className="homestay-preferences-label">Do you smoke?</div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.smoke}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "smoke")
                  }
                />
              </div>
            </Col>
            <Col size="4">
              <div className="homestay-preferences-label">
                Do you have any food restrictions?
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.foodRestrictions}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "foodRestrictions")
                  }
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col size="4">
              <div className="homestay-preferences-label">
                Do you have any allergies?
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.alergies}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "alergies")
                  }
                />
              </div>
            </Col>
            <Col size="4">
              <div className="homestay-preferences-label">
                Do you have any medical conditions
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={selectOptions}
                  selected={accommodation.medicalConditions}
                  onChange={value =>
                    handleHomeStayPreferenceChange(value, "medicalConditions")
                  }
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  nextStep = () => {
    const { nextStep, showPreferences } = this.props;
    if (!showPreferences || !this.validateForm()) {
      nextStep();
    }
  };

  render() {
    const {
      course,
      onClickHomestayPreference,
      showPreferences,
      checked,
      prevStep
    } = this.props;
    return (
      <div className="booking-course-accommodation">
        <p className="heading-4">ACCOMMODATION</p>
        <Row>
          <Col>
            <RadioButton
              label="I'll arrange accommodation myself"
              checked={checked === this.OPTION1 ? true : false}
              onChange={() => onClickHomestayPreference(this.OPTION1)}
            />
          </Col>
        </Row>
        {course.homestays.length > 0 && (
          <div>
            <Row>
              <Col size="4">
                <RadioButton
                  label="Homestay for students younger 18"
                  checked={checked === this.OPTION2 ? true : false}
                  onChange={() => onClickHomestayPreference(this.OPTION2)}
                />
              </Col>
              <Col size="4">
                <span>
                  @{" "}
                  <Currency currencyType={course.currencyType}>
                    {course.homestays[0].under18Price}
                  </Currency>
                  p/w +{" "}
                  <Currency currencyType={course.currencyType}>
                    {course.homestays[0].homestayFee}
                  </Currency>{" "}
                  fee
                </span>
              </Col>
            </Row>
            <Row>
              <Col size="4">
                <RadioButton
                  label="Homestay for students 18 and above"
                  checked={checked === this.OPTION3 ? true : false}
                  onChange={() => onClickHomestayPreference(this.OPTION3)}
                />
              </Col>
              <Col size="4">
                <span>
                  @{" "}
                  <Currency currencyType={course.currencyType}>
                    {course.homestays[0].above18Price}
                  </Currency>
                  p/w +{" "}
                  <Currency currencyType={course.currencyType}>
                    {course.homestays[0].homestayFee}
                  </Currency>{" "}
                  fee
                </span>
              </Col>
            </Row>
          </div>
        )}
        {showPreferences && this.getHomestayPreferences()}
        <div className="button-block">
          <Button
            size={Button.size.MEDIUM}
            type={Button.type.SECONDARY}
            onClick={prevStep}
          >
            STEP BACK
          </Button>

          <Button
            size={Button.size.MEDIUM}
            type={Button.type.SECONDARY}
            onClick={this.nextStep.bind(this)}
          >
            NEXT STEP
          </Button>
        </div>
      </div>
    );
  }
}

export default AccomodationForm;
