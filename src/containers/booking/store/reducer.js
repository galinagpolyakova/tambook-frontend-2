// @flow
import type { Action } from "../../../shared/types/ReducerAction";

import {
  BOOK_A_COURSE_INIT,
  BOOK_A_COURSE_SUCCESS,
  BOOK_A_COURSE_FAILURE,
  FINALIZE_PAYMENT_SUCCESS,
  FINALIZE_PAYMENT_FAILURE,
  FINALIZE_PAYMENT_INIT,
  SHOW_PAYMENT_ERROR
} from "./actionTypes";

export type BookingState = {
  loading: boolean,
  isBookingSuccess: boolean
};

const intialState: BookingState = {
  loading: false,
  isPaymentLoading: false,
  isBookingSuccess: false,
  isPaymentSuccess: false,
  bookingId: null,
  bookingPayload: {},
  error: null
};

function bookACourseInit(state) {
  return {
    ...state,
    loading: true
  };
}
function bookACourseSuccess(state, { bookingId, bookingPayload }) {
  return {
    ...state,
    isBookingSuccess: true,
    loading: false,
    bookingId,
    bookingPayload
  };
}
function bookACourseFailure(state) {
  return {
    ...state,
    loading: false
  };
}

function finalizePaymentInit(state) {
  return {
    ...state,
    error: null,
    isPaymentLoading: true
  };
}

function finalizePaymentSuccess(state) {
  return {
    ...state,
    isPaymentLoading: false,
    isPaymentSuccess: true
  };
}

function finalizePaymentFailure(state, { error }) {
  return {
    ...state,
    error,
    isPaymentLoading: false
  };
}

function showPaymentError(state, { error }) {
  return {
    ...state,
    error
  };
}

const reducer = (
  state: BookingState = intialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case BOOK_A_COURSE_INIT:
      return bookACourseInit(state);
    case BOOK_A_COURSE_SUCCESS:
      return bookACourseSuccess(state, payload);
    case BOOK_A_COURSE_FAILURE:
      return bookACourseFailure(state);
    case FINALIZE_PAYMENT_SUCCESS:
      return finalizePaymentSuccess(state);
    case FINALIZE_PAYMENT_INIT:
      return finalizePaymentInit(state);
    case FINALIZE_PAYMENT_FAILURE:
      return finalizePaymentFailure(state, payload);
    case SHOW_PAYMENT_ERROR:
      return showPaymentError(state, payload);
    default:
      return state;
  }
};

export default reducer;
