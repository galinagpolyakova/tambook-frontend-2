// @flow
import { combineReducers } from "redux";
import course, { type CourseState } from "../containers/course/store/reducer";
import review, { type ReviewState } from "../containers/review/store/reducer";
import auth, { type AuthState } from "../containers/auth/store/reducer";
import booking, {
  type BookingState
} from "../containers/booking/store/reducer";
import profile, {
  type ProfileState
} from "../containers/profile/store/reducer";
import general, {
  type GeneralState
} from "../containers/general/store/reducer";
import blog, { type BlogState } from "../containers/blog/store/reducer";

export type ApplicationState = {
  course: CourseState,
  auth: AuthState,
  review: ReviewState,
  booking: BookingState,
  profile: ProfileState,
  general: GeneralState,
  blog: BlogState
};

export default combineReducers({
  course,
  review,
  auth,
  booking,
  profile,
  general,
  blog
});
